/*
$DESCRIPTION		: This file contains definitions for GPIO functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_HAL_GPIO_H
#define __BLUEBRAIN_HAL_GPIO_H



#ifdef __cplusplus
	extern "C" {
#endif
	 
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
		 
#define GPIO_0		USER_GPIO0_0_Pin
#define GPIO_1		USER_GPIO0_1_Pin
#define GPIO_2		USER_GPIO0_2_Pin
#define GPIO_3		USER_GPIO0_3_Pin
#define GPIO_4		USER_GPIO_4_Pin
#define GPIO_5		USER_GPIO_5_Pin
#define GPIO_6		USER_GPIO_6_Pin
#define GPIO_7		USER_GPIO_7_Pin

#define GPIO_NUMBER           ((uint32_t)8)
	 
#define GPIO_Strongdrive
#define GPIO_Weakdrive

//GPIO_makeinput - Converts the GPIO pin to Input pin
void 
GPIO_makeinput(uint32_t pin_no);

//GPIO_makeoutput - Converts the GPIO pin to Output pin
void 
GPIO_makeoutput(uint32_t pin_no);

//GPIO_pullup - Activates the pullup resistor for GPIO pin 
void 
GPIO_pullup(uint32_t pin_no);
		
//GPIO_pulldown - Activates the pulldown resistor for GPIO pin
void 
GPIO_pulldown(uint32_t pin_no);
		
//GPIO_Opendrain - Converts the GPIO pin to OpenDrain mode
void 
GPIO_Opendrain(uint32_t pin_no);

//GPIO_Tristate - Converts the GPIO pin to Push-Pull/Tristate mode
void 
GPIO_Tristate(uint32_t pin_no);
		
//GPIO_set - Sets the GPIO pin High (1)
void 
GPIO_set (uint32_t pin_no);

//GPIO_reset - Sets the GPIO pin Low (0)		
void 
GPIO_reset (uint32_t pin_no);

//GPIO_toggle - Flips the value of the GPIO pin between High and Low
void 
GPIO_toggle (uint32_t pin_no);

//GPIO_read_pin - Checks if the pin is in High/Low state
bool 
GPIO_read_pin(uint16_t pin_no);
	 
#ifdef __cplusplus
}
#endif

#endif /*__BLUEBRAIN_HAL_GPIO_H */

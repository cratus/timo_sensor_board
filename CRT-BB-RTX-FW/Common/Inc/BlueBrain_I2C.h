/*
$DESCRIPTION		: This file contains definitions for I2C functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_HAL_I2C_H
#define __BLUEBRAIN_HAL_I2C_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "singleton_template.h"

class I2C : public SingletonTemplate<I2C>
{
		public:
						/// Initializes I2C at the given @param speedInKhz
						bool init(unsigned int speedInKhz=400);
						bool start();
						bool stop();
						bool restart();
						bool sendbyte(uint16_t DevAddress, uint8_t pData);
						bool getack();				// Implementation is not done
						bool readbyte(uint16_t DevAddress, uint8_t pData);
						bool giveack();				// Implementation is not done
						bool givenack();			// Implementation is not done
						
						bool write(uint16_t DevAddress, uint8_t* pData, uint16_t Size);
						bool read(uint16_t DevAddress, uint8_t* pData, uint16_t Size);

		private:
						I2C_HandleTypeDef* handle_i2c;
						I2C(){} ///< Private constructor for this singleton class
						friend class SingletonTemplate<I2C>;  ///< Friend class used for Singleton Template
};
							
class Sensor_I2C : public SingletonTemplate<Sensor_I2C>
{
		public:
						/// Initializes I2C at the given @param speedInKhz
						bool init(unsigned int speedInKhz=400);
				
		private:
						I2C_HandleTypeDef* sensor_i2c;
						Sensor_I2C(){} ///< Private constructor for this singleton class
						friend class SingletonTemplate<Sensor_I2C>;  ///< Friend class used for Singleton Template
};
		
/**
	* I2C Addresses for on-board devices attached to I2C Bus
*/

enum BlueBrain_I2C_Device_Addresses{
		I2CAddr_IMU 				= 0x38,			// xxx:
		I2CAddr_TempSensor  = 0x90,			// xxx: 
		I2CAddr_LDCSensor   = 0x40,			// xxx:
};
		
#endif /*__BLUEBRAIN_HAL_I2C_H */

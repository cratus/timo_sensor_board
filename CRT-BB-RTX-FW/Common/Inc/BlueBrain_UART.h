/*
$DESCRIPTION		: This file contains definitions for UART functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_HAL_UART_H
#define __BLUEBRAIN_HAL_UART_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stdbool.h"
#include "singleton_template.h"

#ifndef TERMINAL_ENABLE
	/* CONSOLE PRINTS -> Porting printf function to different UART ports */
	#define BLE_CONSOLE									0
	#define DEBUG_CONSOLE								1
	#define USER_CONSOLE								0
#endif	 

#define UART_BAUD_9600				((uint32_t)9600)
#define UART_BAUD_19200				((uint32_t)19200)
#define UART_BAUD_38400				((uint32_t)38400)
#define UART_BAUD_76800				((uint32_t)76800)
#define UART_BAUD_115200	 		((uint32_t)115200)
	
#define UART_BITS_8						UART_WORDLENGTH_8B
#define UART_BITS_9						UART_WORDLENGTH_9B
	 
#define UART_STOP_1						UART_STOPBITS_1
#define UART_STOP_2						UART_STOPBITS_2

extern UART_HandleTypeDef huart1;


enum uart_print_type_t {
	UART_EOL_NULL = 0,
	UART_EOL_CR,
	UART_EOL_LF,
	UART_EOL_CRLF
};
	
class Debug_UART : public SingletonTemplate<Debug_UART>
{
	public:
			// Initialize this device, @returns true if successful
			HAL_StatusTypeDef init(uint32_t baudrate, uint32_t parity, uint32_t databits, uint32_t stopbits); 						
			UART_HandleTypeDef* get_uart(void);

			bool raw_print(uint8_t *pData);
			bool new_print(uint8_t pData);
			HAL_StatusTypeDef print(const char *pData);
			HAL_StatusTypeDef print(uint32_t num);
			HAL_StatusTypeDef print(int32_t lcd_num);
			HAL_StatusTypeDef println(const char *pData);
			HAL_StatusTypeDef read(uint8_t *pData, uint16_t Size);
			bool readln(uint8_t *pData, uint16_t Size);
			
	private:
			UART_HandleTypeDef debug_uart;
			uint8_t print_type;
			bool echo;
			Debug_UART() {
						print_type = UART_EOL_CRLF;
						echo = false;
			}
			friend class SingletonTemplate<Debug_UART>;
				
};
	
class User_UART : public SingletonTemplate<User_UART>
{
	public:
			// Initialize this device, @returns true if successful
			HAL_StatusTypeDef init(uint32_t baudrate, uint32_t parity, uint32_t databits, uint32_t stopbits);		
			UART_HandleTypeDef* get_uart(void);
			HAL_StatusTypeDef print(const char *pData);
			HAL_StatusTypeDef print(unsigned int num);
			HAL_StatusTypeDef print(signed int lcd_num);
			HAL_StatusTypeDef println(const char *pData);
			bool print_EOL(uint8_t EOL_setting);
			void print_echo(bool on);
			HAL_StatusTypeDef read(uint8_t *pData, uint16_t Size);
			bool readln(uint8_t *pData, uint16_t Size);
			
	private:
			UART_HandleTypeDef user_uart;
			uint8_t print_type;
			bool echo;
			User_UART() { 
						print_type = UART_EOL_CRLF;
						echo = false;
			}
			friend class SingletonTemplate<User_UART>;
				
};
	
class BLE_UART : public SingletonTemplate<BLE_UART>
{
	public:
			// Initialize this device, @returns true if successful
			HAL_StatusTypeDef init(uint32_t baudrate, uint32_t parity, uint32_t databits, uint32_t stopbits);		
			UART_HandleTypeDef* get_uart(void);
			HAL_StatusTypeDef print(const char *pData);
			HAL_StatusTypeDef print(unsigned int num);
			HAL_StatusTypeDef print(signed int lcd_num);
			HAL_StatusTypeDef println(uint8_t *pData);
			bool print_EOL(uint8_t EOL_setting);
			void print_echo(bool on);
			HAL_StatusTypeDef read(uint8_t *pData, uint16_t Size);
			bool readln(uint8_t *pData, uint16_t Size);
			bool Kick_BLE();
			bool send_data(uint8_t *pData, uint8_t Size); 
			
	private:
			UART_HandleTypeDef ble_uart;
			uint8_t print_type;
			bool echo;
			BLE_UART() { 
						print_type = UART_EOL_CRLF;
						echo = false;
			}
			friend class SingletonTemplate<BLE_UART>;		
};

#endif /*__BLUEBRAIN_HAL_UART_H */

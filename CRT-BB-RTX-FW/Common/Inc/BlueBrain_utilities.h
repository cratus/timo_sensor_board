/*
$DESCRIPTION		: This file contains definitions for I2C functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_HAL_UTILITIES_H
#define __BLUEBRAIN_HAL_UTILITIES_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"

static inline void delay(uint32_t Delay) { osDelay(Delay); }

#endif /*__BLUEBRAIN_HAL_UTILITIES_H */

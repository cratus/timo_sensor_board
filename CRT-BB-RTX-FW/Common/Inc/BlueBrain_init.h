/*
$DESCRIPTION		: This file contains definitions for initialization of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_HAL_INIT_H
#define __BLUEBRAIN_HAL_INIT_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

void BlueBrain_init(void);

#ifdef __cplusplus
}
#endif

#endif /*__BLUEBRAIN_HAL_INIT_H */

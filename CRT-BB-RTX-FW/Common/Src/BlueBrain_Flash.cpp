/*
$DESCRIPTION		: This file contains definitions for SPI functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-14 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

#include "BlueBrain_Flash.h"
#include "slld_util.h"
#include "BlueBrain_IO.h"
#include "slld_hal.h"

//#define PAGE_SIZE 256

#define CMD_GET_ID      0x9f
#define CMD_BLOCK_ERASE 0x20
#define CMD_DEV_ERASE   0x60
#define CMD_PROGRAM     0x02
#define CMD_READ        0x0B
#define CMD_GET_SR      0x05
#define CMD_SET_SR      0x01
#define CMD_WRITE_EN    0x06
#define CMD_WRITE_DIS   0x04


unsigned char Flash::readID(void)
{
		unsigned char lid[8]={0,0,0,0,0,0,0,0};
		if(slld_RPWDCmd(lid)==SLLD_OK)
			return lid[7];
		else
			return 0;
}

bool Flash::print(void)
{
		unsigned char hello[8] = {0,1,0,1,0,1,0,1};
		unsigned char trial[8] = {20,21,20,21,20,21,20,21};
		slld_ProgramFlash(0x00000400,hello,8,256,PROG_PAGE);
		slld_ReadFlash(0x00000400,trial,8,256,NORMAL_READ);
		uint8_t status = FLASH_READ(0x0C,0x00000400,trial,6);
		if(status==SLLD_OK)
		{
		USER_UART.print(trial[0]);
		USER_UART.print(" ");
		USER_UART.print(trial[1]);
		USER_UART.print(" ");
		USER_UART.print(trial[2]);
		USER_UART.print(" ");
		USER_UART.print(trial[3]);
		USER_UART.print(" ");
		USER_UART.print(trial[4]);
		USER_UART.print(" ");
		USER_UART.print(trial[5]);
		USER_UART.println(" ");
		}
		return true;
}

bool Flash :: write_enable(void)
{
	uint8_t status = HAL_SPI_Transmit(&hspi2, (uint8_t*)CMD_WRITE_EN, 1, 100);
	if(status==HAL_OK)
		return true;
	else
		return false;
	
}


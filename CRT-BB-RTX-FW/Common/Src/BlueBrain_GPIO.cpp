/*
$DESCRIPTION		: This file contains definitions for GPIO functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

#include <stdbool.h>
#include "BlueBrain_GPIO.h"

//GPIO_makeinput - Converts the GPIO pin to Input pin
void 
GPIO_makeinput(uint32_t pin_no)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	/* GPIO Ports Clock Enable */
  __GPIOC_CLK_ENABLE();
	
	/*Configure GPIO pins : USER_GPIO0_0_Pin USER_GPIO0_1_Pin USER_GPIO0_2_Pin USER_GPIO0_3_Pin 
                           USER_GPIO_4_Pin USER_GPIO_5_Pin USER_GPIO_6_Pin USER_GPIO_7_Pin 
  */
  GPIO_InitStruct.Pin = pin_no;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

//GPIO_makeoutput - Converts the GPIO pin to Output pin
void 
GPIO_makeoutput(uint32_t pin_no)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	/* GPIO Ports Clock Enable */
  __GPIOC_CLK_ENABLE();
	
	/*Configure GPIO pins : USER_GPIO0_0_Pin USER_GPIO0_1_Pin USER_GPIO0_2_Pin USER_GPIO0_3_Pin 
                           USER_GPIO_4_Pin USER_GPIO_5_Pin USER_GPIO_6_Pin USER_GPIO_7_Pin 
  */
  GPIO_InitStruct.Pin = pin_no;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;		//Push Pull mode
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
}

//GPIO_pullup - Activates the pullup resistor for GPIO pin 
void 
GPIO_pullup(uint32_t pin_no)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = pin_no;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

//GPIO_pulldown - Activates the pulldown resistor for GPIO pin
void 
GPIO_pulldown(uint32_t pin_no)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = pin_no;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

//GPIO_Opendrain - Converts the GPIO pin to OpenDrain mode
void 
GPIO_Opendrain(uint32_t pin_no)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = pin_no;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;	// Open Drain mode	
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

//GPIO_Tristate - Converts the GPIO pin to Push-Pull/Tristate mode
void 
GPIO_Tristate(uint32_t pin_no)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = pin_no;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;	// Push-Pull/Tristate mode
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

//GPIO_set - Sets the GPIO pin High (1)
void 
GPIO_set (uint32_t pin_no)
{
	uint32_t position;
  uint32_t ioposition = 0x00;
  uint32_t iocurrent = 0x00;
	
	for(position = 0; position < GPIO_NUMBER; position++) {
			/* Get the IO position */
			ioposition = ((uint32_t)0x01) << position;
			/* Get the current IO position */
			iocurrent = pin_no & ioposition;

			if(iocurrent == ioposition) {
					GPIOC->BSRR = iocurrent;
			}
	}
}

//GPIO_reset - Sets the GPIO pin Low (0)		
void 
GPIO_reset (uint32_t pin_no)
{
	uint32_t position;
  uint32_t ioposition = 0x00;
  uint32_t iocurrent = 0x00;
	
	for(position = 0; position < GPIO_NUMBER; position++) {
			/* Get the IO position */
			ioposition = ((uint32_t)0x01) << position;
			/* Get the current IO position */
			iocurrent = pin_no & ioposition;

			if(iocurrent == ioposition) {
					GPIOC->BSRR = (iocurrent << 16);
			}
	}
}

//GPIO_toggle - Flips the value of the GPIO pin between High and Low
void 
GPIO_toggle (uint32_t pin_no)
{
	uint32_t position;
  uint32_t ioposition = 0x00;
  uint32_t iocurrent = 0x00;
	
	for(position = 0; position < GPIO_NUMBER; position++)
  {
			/* Get the IO position */
			ioposition = ((uint32_t)0x01) << position;
			/* Get the current IO position */
			iocurrent = pin_no & ioposition;

			if(iocurrent == ioposition) {
					GPIOC->ODR ^= iocurrent;
			}
	}
}

//GPIO_read_pin - Checks if the pin is in High/Low state
bool 
GPIO_read_pin(uint16_t pin_no)
{
	return HAL_GPIO_ReadPin(GPIOC,pin_no);
}

/*
$DESCRIPTION		: This file contains definitions for Utilities functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

#include "BlueBrain_utilities.h"

void 
error_handler(HAL_StatusTypeDef status)
{
		printf("\nGot Error Status: %d\n",status);
		__disable_irq();
		while(1)
		{
			__WFI();
			__WFE();
		}
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void 
assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif
/*    
$DESCRIPTION    : Soucrce file for logger API.   

$Copyright      : CRATUS TECHNOLOGY INC, 2013-16  

$Project        : BlueBrain  

$Author         : chitrang  

$Warnings       : <if any>  
*/


#include <stdlib.h>   // malloc()
#include <stdio.h>    // sprintf()
#include <string.h>   // strlen()
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>

#include "cmsis_os.h" // RTX header file.


#include "ionfs.h"     //  Header file related to Spansion FS 
#include "ion_lib.h"   //	 Header file related to Spansion FS 
#include "ftl_if_ex.h" //  Header file related to Spansion FS 

#include "BlueBrain_FileLogger.h"
#include "stm32f4xx_hal.h"
//#include "RTX_CM_lib.h"
extern RTC_HandleTypeDef hrtc;
static void logger_task(void const *param);  // logger thread prototype

osThreadDef(logger_task, osPriorityIdle, 1, 0);  // Thread1 definition structure

static uint16_t g_blocked_calls = 0;                ///< Number of logging calls that blocked
static uint16_t g_buffer_watermark = 0;             ///< The watermark of the number of buffers consumed
static uint16_t g_highest_file_write_time = 0;      ///< Highest time spend while trying to write file buffer
static char * gp_file_buffer = NULL;                ///< Pointer to local buffer space before it is written to file


osMutexDef (Mutex_logfile);
static osMutexId mutex_id_logfile;  // This mutex id is for the mutex of the log file. 

static osMessageQId write_buffer_q;
static osMessageQId empty_buffer_q;

//static QueueHandle_t g_write_buffer_queue = NULL;   ///< Log message pointers are written to this queue
//static QueueHandle_t g_empty_buffer_queue = NULL;   ///< Log message pointers are available from this queue

static uint32_t g_logger_calls[log_last] = { 0 };   ///< Number of logged messages of each severity

static uint8_t g_logger_printf_mask = (1 << log_debug);

extern bool os_idle_flag;
extern uint32_t os_tick_val(void);


bool Is_osIdle(void){
	return !os_idle_flag;
}


static bool logger_write_to_file(const void * buffer, const uint32_t bytes_to_write)
{
    bool success = false;
		int32_t fd, rtn = 0;
		uint32_t bytes_written = 0;
		stat_t stbuf;
    const uint32_t start_time = 0;
		osStatus status_mutex;

	
	uint32_t tickstart, tickstart1, diff = 0;
  tickstart = os_tick_val();
	//printf("tickstart is : %u \n", tickstart );
	
    if (0 == bytes_to_write) {
        success = true;
    }
 
    /* File not opened, open it, seek it, and then write it */
		
//		status_mutex  = osMutexWait (mutex_id_logfile, 0);
//				if (status_mutex != osOK)  {
//            printf("Error in waiting for mutex \n");
//   		 }
		
		else if ( !(0 > (fd = ionFS_open( _TC(FILE_LOGGER_FILENAME), ION_O_CREAT | ION_O_RDWR  | ION_O_APPEND))))
		{
			rtn = ionFS_fstat( fd, &stbuf );
			if(stbuf.size == (rtn = ionFS_lseek(fd, stbuf.size, SEEK_SET)))
			{
				//printf("Writing the bytes...\n"); 
				bytes_written = ionFS_write( fd, buffer, bytes_to_write );
			}
			
			rtn = ionFS_close( fd );
			if ( 0 > rtn )
			{
				printf("Failed file write: ");
			}
			tickstart1 = os_tick_val();
			
			diff = tickstart1 - tickstart;
			printf("Diff is : %d \n", diff );
//			 status_mutex = osMutexRelease(mutex_id_logfile);
//				if (status_mutex != osOK)  {
//				printf("Error in releasing for mutex \n");
//				}
		}
	
		
		else {
        printf("Failed file write: ");
    }
	

    /* To be successful, bytes written should be the same count as the bytes intended to be written */
    success = (bytes_to_write == bytes_written);
		//printf("SUCESSSSSS!!!!!!!!!   ---> %d \n",success);

    /* We don't want to silently fail, so print a message in case an error occurs */
    if (!success) {
			printf("Error: writing logfile. %u/%u written.\n", (unsigned)bytes_written, (unsigned)bytes_to_write);
    }

    return success;
}


static char * logger_get_buffer_ptr(const bool os_running)
{
    char * buffer = NULL;
		osEvent evt;

    /* Get an available buffer to write the data to, and if OS is not running, we will always get a buffer */
    if (!os_running) {
			
			
			evt = osMessageGet(empty_buffer_q, 0);
			buffer = (char *)evt.value.p;
       // xQueueReceive(g_empty_buffer_queue, &buffer, 0);
    
		}
		else{
		evt = osMessageGet(empty_buffer_q, FILE_LOGGER_BLOCK_TIME_MS);
		
    if (evt.status == osEventMessage){
        ++g_blocked_calls;

        /* This time, just block forever until we get a buffer */
      //printf("Getting an empty space...\n");
			evt = osMessageGet(empty_buffer_q, osWaitForever);
			buffer = (char *)evt.value.p;
    }
	}
    return buffer;
}

static void logger_write_log_message(char * buffer, const bool os_running)
{
    /* Send the buffer to the queue for the logger task to write */
    if (os_running) {
			osEvent evt;
			//printf("Putting the data to write_queue: %s \n", buffer);
			evt.status  = osMessagePut(write_buffer_q, (uint32_t)buffer ,  osWaitForever );
			if(evt.status != osOK)
			{
				printf("Fail to put the message to write queue \n");
			}
        //xQueueSend(g_write_buffer_queue, &buffer, portMAX_DELAY);
    }
		
		else
		{
			size_t len = strlen(buffer);
			buffer[len] = '\n';
      buffer[++len] = '\0';
      logger_write_to_file(buffer, len);

			osEvent evt;
			//printf("Putting the data to write_queue: %s \n", buffer);
			evt.status  = osMessagePut(empty_buffer_q, (uint32_t)buffer , 0 );
			if(evt.status != osOK)
			{
				printf("Fail to put the message to write queue \n");
			}
        /* OS not running, so put the buffer back to the empty queue instead of write queue */
      //  xQueueSend(g_empty_buffer_queue, &buffer, 0);
		}
}
static void logger_task(void const *param)
{
    /* Use "char * const" to disallow this pointer to be moved.
     * We don't want to use "const char *" because we do want to be able to
     * write to this pointer instead of using excessive casts later.
     */
    char * const start_ptr = gp_file_buffer;
    char * const end_ptr = start_ptr + FILE_LOGGER_BUFFER_SIZE;

    char * log_msg = NULL;
    char * write_ptr = start_ptr;
    size_t len = 0;
    size_t buffer_overflow_cnt = 0;
		osEvent evt;
		
    while (1)
    {
        /* Receive the log message we wish to write to our buffer.
         * Timeout or NULL pointer received is the signal to flush the data.
         */
        log_msg = NULL;
				//printf("Is this task running or not.. \n");
				evt = osMessageGet(write_buffer_q, (1000 * FILE_LOGGER_FLUSH_TIME_SEC));
			
                if (evt.status != osEventMessage || NULL == evt.value.p ) {
										//printf("My task failed to receive  from the queue. \n");
										//printf("Failed Status is %d  \n", evt.status);
										logger_write_to_file(start_ptr, (write_ptr - start_ptr));
										write_ptr = start_ptr;
										continue;
                }
			

        /* Update the watermark of the number of messages we see in the queue and note that we
         * add one to account for the message we just dequeued.
         */
//        if ((len = (1 + uxQueueMessagesWaiting(g_write_buffer_queue)) ) > g_buffer_watermark) {
//            g_buffer_watermark = len;
//        }

        /* Get the length and append the newline character */
				log_msg = (char *)evt.value.p;
				len = strlen(log_msg);
				//printf("The length is: %d \n", len);
        log_msg[len] = '\n';
        log_msg[++len] = '\0';
								
				//printf("Inside the logger task. Let's see the buffer--> %s  \n", log_msg);
        /* This is test code to immediately write data to the file.  It is highly in-efficient and is
         * included here just for reference or test purposes.
         */
        #if 0
        {
            logger_write_to_file(log_msg, len);
						osMessagePut(empty_buffer_q, (uint32_t)log_msg ,  osWaitForever );
            //xQueueSend(g_empty_buffer_queue, &log_msg, portMAX_DELAY);
            continue;
        }
        #endif

        /* If we will overflow our buffer we need to write the full buffer and do partial copy */
        if (len + write_ptr >= end_ptr)
        {
            /* This could be zero when we write the last byte in the buffer */
            buffer_overflow_cnt = (len + write_ptr - end_ptr);

            /* Copy the partial message up until the end of the buffer */
            memcpy(write_ptr, log_msg, (end_ptr - write_ptr));

            /* Write the entire buffer to the file */
						
            logger_write_to_file(start_ptr, (end_ptr - start_ptr));

            /* Optional: Zero out the buffer space */
            // memset(start_ptr, '\0', buffer_size);

            /* Copy the left-over message to the start of "fresh" buffer space (after writing to the file) */
            if (buffer_overflow_cnt > 0) {
                memcpy(start_ptr, (log_msg + len - buffer_overflow_cnt), buffer_overflow_cnt);
            }
            write_ptr = start_ptr + buffer_overflow_cnt;
        }
        /* Buffer has enough space, write the entire message to the buffer */
        else {
            memcpy(write_ptr, log_msg, len);
            write_ptr += len;
        }

        /* Put the data pointer back to the available buffers */
        //xQueueSend(g_empty_buffer_queue, &log_msg, portMAX_DELAY);
				//printf("ARE WE COMING HERE 1--- \n");
				osEvent evt;
				evt.status = osMessagePut(empty_buffer_q, (uint32_t)log_msg ,  osWaitForever );
				if(evt.status != osOK)
				{
					printf("Failed to put into the empty queue. \n");
				}
    }
}


static bool logger_internal_init(unsigned long logger_priority)
{
    uint32_t i = 0;
    char * ptr = NULL;
		char * rptr = NULL;
    const bool success = true;
	    
 
		mutex_id_logfile = osMutexCreate  (osMutex (Mutex_logfile));
	
		
		
	
	/* Create the queues that keep track of the written buffers, and available buffers */
		osMessageQDef(write_buffer_q,FILE_LOGGER_NUM_BUFFERS, sizeof((char *)) );
		write_buffer_q = osMessageCreate(osMessageQ(write_buffer_q), NULL);
		
		osMessageQDef(empty_buffer_q,FILE_LOGGER_NUM_BUFFERS, sizeof((char *)) );
		empty_buffer_q = osMessageCreate(osMessageQ(empty_buffer_q), NULL);

    /* Create the buffer space we write the logged messages to (before we flush it to the file) */
    gp_file_buffer = (char*) malloc(FILE_LOGGER_BUFFER_SIZE);
	
		if(NULL == mutex_id_logfile)
			{
			goto failure;
			}
		
    if (NULL == gp_file_buffer) {
				printf("Failure - 1..\n");
        goto failure;
    }

    //g_write_buffer_queue = xQueueCreate(FILE_LOGGER_NUM_BUFFERS, sizeof(char*));
    //g_empty_buffer_queue = xQueueCreate(FILE_LOGGER_NUM_BUFFERS, sizeof(char*));
		
		
    if (NULL == write_buffer_q || NULL == empty_buffer_q) {
				printf("Failure - 2..\n");
        goto failure;
    }

    /* Create the actual buffers for log messages */
    for (i = 0; i < FILE_LOGGER_NUM_BUFFERS; i++)
    {
        ptr = (char*) malloc(FILE_LOGGER_LOG_MSG_MAX_LEN);

        if (NULL == ptr) {
						printf("Failure - 3..\n");
            goto failure;
        }

        /* DO NOT USE xQueueSendFromISR().
         * It causes weird file write errors and corrupts the entire file system
         * when the logger task is running.
         */
				
				osMessagePut(empty_buffer_q, (uint32_t)ptr ,  0 );      // Not sure 
				
			}
       // xQueueSend(g_empty_buffer_queue, &ptr, 0);
			osThreadId log_thread_id;
			log_thread_id = osThreadCreate(osThread(logger_task), NULL);
				
			 if (log_thread_id == NULL) { 
					printf("Failure to create thread../n");
  		   goto failure;
				}

//    if (!xTaskCreate(logger_task, "logger", FILE_LOGGER_STACK_SIZE, NULL, logger_priority, NULL))
//    {
//        goto failure;
//    }

    return success;
		osEvent evt;
    /* failure case to delete allocated memory */
    failure:
        if (gp_file_buffer) {
            free(gp_file_buffer);
            gp_file_buffer = NULL;
        }

        if (empty_buffer_q) {
            for (i = 0; i < FILE_LOGGER_NUM_BUFFERS; i++) {
							evt = osMessageGet(empty_buffer_q, 0);
                if (evt.status == osOK) {
										rptr = (char *)evt.value.p;
                    free (rptr);
                }
            }
        }

        /* Delete g_write_buffer_queue */
        /* Delete g_empty_buffer_queue */

        return (!success);
}

static bool logger_initialized(void)
{
    return (NULL != gp_file_buffer);
}


uint32_t logger_get_logged_call_count(logger_msg_t severity)
{
    return (severity < log_last) ? g_logger_calls[severity] : 0;
}

uint16_t logger_get_blocked_call_count(void)
{
    return g_blocked_calls;
}

uint16_t logger_get_highest_file_write_time_ms(void)
{
    return g_highest_file_write_time;
}

uint16_t logger_get_num_buffers_watermark(void)
{
    return g_buffer_watermark;
}

void logger_init(uint8_t logger_priority)
{
    /* Prevent double init */
    if (!logger_initialized())
    {
        if (!logger_internal_init(logger_priority)) {
            printf("ERROR: logger initialization failure\n");
        }
    }
}
void logger_set_printf(logger_msg_t type, bool enable)
{
    const uint8_t mask = (1 << type);
    if (enable) {
        g_logger_printf_mask |= mask;
    }
    else {
        g_logger_printf_mask &= ~mask;
    }
}

void logger_log(logger_msg_t type, const char * filename, const char * func_name, unsigned line_num,
                const char * msg, ...)
{
    if (!logger_initialized()) {
        return;
    }

    uint32_t len = 0;
    char * buffer = NULL;
    char * temp_ptr = NULL;
    const bool os_running =  0 ;//Is_osIdle(); // osKernelRunning();
		
		RTC_TimeTypeDef curr_time_stamp;
		RTC_DateTypeDef curr_date_stamp;
		if(HAL_RTC_WaitForSynchro(&hrtc) == HAL_OK)
		{
		HAL_RTC_GetTime(&hrtc,&curr_time_stamp,RTC_FORMAT_BIN);
		HAL_RTC_GetDate(&hrtc,&curr_date_stamp,RTC_FORMAT_BIN);
		}

    /* This must match up with the logger_msg_t enumeration */
    const char * const type_str[] = { "debug", "info", "warn", "error" };

    // Find the back-slash or forward-slash to get filename only, not absolute or relative path
    if(0 != filename) {
        temp_ptr = (char *)strrchr(filename, '/');
        // If forward-slash not found, find back-slash
        if(0 == temp_ptr) temp_ptr = (char *) strrchr(filename, '\\');
        if(0 != temp_ptr) filename = temp_ptr+1;
    }
    else {
        filename = "";
    }

    if (0 == func_name) {
        func_name = "";
    }

    /* Get an available buffer */
    buffer = logger_get_buffer_ptr(os_running);

    do {
        int mon = curr_date_stamp.Month;
				int date = curr_date_stamp.Date;
        int hr = curr_time_stamp.Hours;
        int min =  curr_time_stamp.Minutes;
        int sec = curr_time_stamp.Seconds;
        const char *log_type_str = type_str[type];
        const char *func_parens  = func_name[0] ? "()" : "";

        /* Write the header including time, filename, function name etc */
        len = sprintf(buffer, "%d/%d,%02d:%02d:%02d,%s,%s,%s%s,%u,",
                      mon, date, hr, min, sec, log_type_str, filename, func_name, func_parens, line_num);
    } while (0);

    /* Append actual user message, and leave one space for \n to be appended by the logger task.
     * There is no efficient way to append \n here since we will have to use strlen(),
     * but since the logger task will take strlen() anyway, it can append it there.
     *
     * Example: max length = 10, and say we printed 5 chars so far "hello"
     *          we will sprintf "world" to "hello" where n = 10-5-1 = 4
     *          So, this sprintf will append and make it: "hellowor\0" leaving space to add \n
     *
     * Note: You cannot use returned value from vsnprintf() because snprintf() returns:
     *       "number of chars that would've been printed if n was sufficiently large"
     *
     * Note: "size" of snprintf() includes the NULL character
     */
    do {
        va_list args;
        va_start(args, msg);
        vsnprintf(buffer + len, FILE_LOGGER_LOG_MSG_MAX_LEN-len-1, msg, args);
        va_end(args);
    } while (0);

    ++g_logger_calls[type];
		
		//printf("Let's see msg: %s and the buffer: %s   \n", msg, buffer);
    logger_write_log_message(buffer, os_running);
		
		os_idle_flag = false;
}

void logger_log_raw(const char * msg, ...)
{
    if (!logger_initialized()) {
        return;
    }

    const bool os_running =  Is_osIdle(); // osKernelRunning();
    char * buffer = logger_get_buffer_ptr(os_running);

    /* Print the actual user message to the buffer */
    do {
        va_list args;
        va_start(args, msg);
        vsnprintf(buffer, FILE_LOGGER_LOG_MSG_MAX_LEN-1, msg, args);
        va_end(args);
    } while (0);

    logger_write_log_message(buffer, os_running);
		os_idle_flag = false;
}

/*    
$DESCRIPTION    : Soucrce file for File API.   

$Copyright      : CRATUS TECHNOLOGY INC, 2013-16  

$Project        : BlueBrain  

$Author         : chitrang  

$Warnings       : <if any>  
*/

#include "Bluebrain_FileAPI.h"

bool BB_File_API :: f_read(const char *pFilename, void *pData, unsigned int bytesToRead)
{
	bool status = false;
	int32_t fd, rtn;
	
	fd = ionFS_open( _TC(pFilename), ION_O_RDWR );
	if ( 0 > fd )
	{
	printf( "ionFS_open() fail, 0x%x", ionFS_get_errno() );
	return status;
	}
	
	rtn = ionFS_read( fd, pData , bytesToRead );
	
	if ( bytesToRead != rtn )
		printf( "ionFS_read() fail, 0x%x", ionFS_get_errno() );
	
	rtn = ionFS_close( fd );
	
	status = true;
	
	if ( 0 > rtn )
		printf( "ionFS_close() fail, 0x%x", ionFS_get_errno() );	
	


	return status;
}


bool BB_File_API :: f_write(const char *pFilename, void *pData, unsigned int bytesToWrite)
{
	bool status = false;
	int32_t fd, rtn;
	
	fd = ionFS_open( _TC(pFilename), ION_O_RDWR );
	if ( 0 > fd )
	{
	printf( "ionFS_open() fail, 0x%x", ionFS_get_errno() );
	return status;
	}
	
	rtn = ionFS_write( fd, pData , bytesToWrite );
	
	if ( bytesToWrite != rtn )
		printf( "ionFS_read() fail, 0x%x", ionFS_get_errno() );
	
	rtn = ionFS_close( fd );
	
	status = true;
	
	if ( 0 > rtn )
		printf( "ionFS_close() fail, 0x%x", ionFS_get_errno() );	
	
	return status;
}


bool BB_File_API ::  f_create(const char* pFilename, mod_t mode)
{
	int32_t fd, rtn;
	
	fd = ionFS_creat( _TC(pFilename), mode);
	if ( 0 > fd )
		{
		printf( "ionFS_create() fail, 0x%x", ionFS_get_errno() );
		return false;
		}
		
	rtn = ionFS_close( fd );
	if ( 0 > rtn )
	printf( "ionFS_close() fail, 0x%x", ionFS_get_errno() );

	return true;
}


bool BB_File_API :: f_append(const char *pFilename, void *pData, unsigned int bytesToAppend)
{
	bool status = false;
	int32_t fd, rtn;
	
	fd = ionFS_open( _TC(pFilename), ION_O_RDWR | ION_O_APPEND );
	if ( 0 > fd )
	{
	printf( "ionFS_open() fail, 0x%x", ionFS_get_errno() );
	return status;
	}
	
	rtn = ionFS_write( fd, pData , bytesToAppend );
	
	if ( bytesToAppend != rtn )
		printf( "ionFS_read() fail, 0x%x", ionFS_get_errno() );
	
	rtn = ionFS_close( fd );
	
	status = true;
	
	if ( 0 > rtn )
		printf( "ionFS_close() fail, 0x%x", ionFS_get_errno() );	
	
	return status;
}



bool BB_File_API :: f_delete(const char *pFilename)
{
	int32_t fd, rtn;
	rtn = ionFS_unlink( _TC(pFilename) );
	if ( 0 > rtn )
		{
		printf( "ionFS_unlink() fail, 0x%x", ionFS_get_errno() );
		return false;
		}
	return true;
}

void BB_File_API :: read_whole_file(const char *pFilename)
{
	char buffer[10];
	stat_t stbuf;
	uint32_t filesizeInBytes = 0;
	int32_t rtn, fd;
	uint32_t numForloop = 0;
	uint8_t residual = 0;
	int32_t j =0 ;
	fd = ionFS_open( _TC(pFilename), ION_O_RDWR);
	if ( 0 > fd )
		printf( "ionFS_open() fail, 0x%x \n ", ionFS_get_errno());
	
	ionFS_fstat( fd, &stbuf );
	
	filesizeInBytes = stbuf.size;
	
	ionFS_lseek( fd, 0 , SEEK_SET);
	
	if(filesizeInBytes >= 10)
		{
		  numForloop = filesizeInBytes / 10; 
			residual = filesizeInBytes % 10;
			for(int i = 1 ;  i <= numForloop; i++)
			{
				j = i* 10;
				ionFS_read( fd, buffer , 10);
				printf("%s", buffer);
				ionFS_lseek( fd, j , SEEK_CUR);
			}
		}
	
		else
		{
			ionFS_read( fd, buffer , filesizeInBytes);
			printf("%s", buffer);
		}

}

#include "TIMO.h"
#include "stdint.h"
#include "main.h"
#include "cmsis_os.h"
#include "RS_485.h"
#include <time.h>

#ifdef __cplusplus
extern "C"
{
#endif 

  void RTC_Alarm_IRQHandler(void);

#ifdef __cplusplus
}
#endif

/*
 *
 * User Uart Task
 *
 *
 */

static timoPacket 
  txPacket,
	rxPacket;
static uint8_t rxCounter;
static uint8_t *rxPkt;

static RTC_TimeTypeDef curr_time_stamp;
static RTC_DateTypeDef curr_date_stamp;
static uint8_t rx_user_uart_state;
static uint8_t networkAddress;

static uint8_t readNetworkAddress(void);


/* test */
static RTC_TimeTypeDef temp2_time_stamp;
static RTC_DateTypeDef temp2_date_stamp;
/* test */

static RTC_TimeTypeDef temp_time_stamp;
static RTC_DateTypeDef temp_date_stamp;

#define networkAddrReadGPIOfirst (GPIO_0)

static  int rxCharRS485;

void UserUartTask(void const *params)
{
  osEvent evt;
	uint8_t prev_sec = 61;
	/* 
	 * Read network address from GPI0_0 thru GPIO_4
	 */
	networkAddress = readNetworkAddress();
	
	printf("Network Address: %02X\n", networkAddress);
	
	for (rx_user_uart_state = 0;;)
	{	
//	  printf ("\nwaiting for event\n");
		evt = osSignalWait (0x0001, osWaitForever); // wait forever for the signal (0x0001)
//		printf("evt.status: %08X\n", evt.status);
		if (evt.status != osEventSignal)
			continue;
	
		while ((rxCharRS485 = RS_485_getChar()) != -1)
		{	
  		switch (rx_user_uart_state)
		  {
			  case 0:
			  	if (networkAddress == (rxCharRS485 & RS_485_NETWORK_MASK))
				  {				
					  //printf("rxCharRS485 & ~RS_485_NETWORK_MASK: %02X\n", rxCharRS485 & ~RS_485_NETWORK_MASK);
					
					  switch (rxCharRS485 & ~RS_485_NETWORK_MASK)
					  {
						  case RS_485_ROUND_TRIP: // echo round-trip character  
							
#ifdef JITTER_TEST
	          	  GPIO_set(GPIO_7);
#endif									
							  printf("RS_485_ROUND_TRIP\n");
							  RS_485_sendAddressCharacter(rxCharRS485); // echo it back to Coordinator Board for round-trip-time test
		
#ifdef JITTER_TEST
		            GPIO_reset(GPIO_7);
#endif							
							  break;
						
						  case RS_485_GET_TIME:
							  printf("RS_485_GET_TIME\n");
						
						    txPacket.address = rxCharRS485;
							
							  HAL_RTC_GetTime(&hrtc,&curr_time_stamp,RTC_FORMAT_BIN);
							  HAL_RTC_GetDate(&hrtc,&curr_date_stamp,RTC_FORMAT_BIN);
						
						    txPacket.hms   = curr_time_stamp.Hours   *(60*60) +
							                  (curr_time_stamp.Minutes *60) +
						                     curr_time_stamp.Seconds;		
							  txPacket.sub_seconds = (uint16_t)(curr_time_stamp.SubSeconds);						

							  RS_485_sendPacket(txPacket);
						    break;
							 
						  case RS_485_SET_TIME:
						  	//printf("RS_485_SET_TIME\n");
							  LE.toggle(2);
							  rxCounter = 1;
						    rxPkt = (std::uint8_t *)&rxPacket;
						    *rxPkt++ = rxCharRS485;
						  
							  rx_user_uart_state = 1;
							  break;
						
						  default:
							  break;
					  } // end switch (rxCharRS485 & ~RS_485_NETWORK_MASK)
				  } // end if (networkAddress == (rxCharRS485 & RS_485_NETWORK_MASK))
				  break;
				
        case 1:
			  	*rxPkt++ = rxCharRS485;
			    if (rxCounter >= sizeof (rxPacket))
				  {
					  //
					  // Set time
					  //
					  curr_date_stamp.Date = 1;
					  curr_date_stamp.Month = 0;
					  curr_date_stamp.Year = 0;
				
					  curr_time_stamp.Hours   =  rxPacket.hms / (60*60);		
					  curr_time_stamp.Minutes = (rxPacket.hms / 60) % 60;
					  curr_time_stamp.Seconds =  rxPacket.hms % 60;
				
					  HAL_RTC_SetTime(&hrtc,&curr_time_stamp,RTC_FORMAT_BIN);
					  HAL_RTC_SetDate(&hrtc,&curr_date_stamp,RTC_FORMAT_BIN);				
#if 0
            printf("set hms: %02d:%02d:%02d.%05d\n", 
					    curr_time_stamp.Hours,
					    curr_time_stamp.Minutes,
					    curr_time_stamp.Seconds,
              curr_time_stamp.SubSeconds
					  );	
#endif

#if 0
				    /* test */
					  HAL_RTC_GetTime(&hrtc,&temp2_time_stamp,RTC_FORMAT_BIN);
					  HAL_RTC_GetDate(&hrtc,&temp2_date_stamp,RTC_FORMAT_BIN);
					
            printf("get hms: %02d:%02d:%02d.%05d\n", 
					    temp2_time_stamp.Hours,
					    temp2_time_stamp.Minutes,
					    temp2_time_stamp.Seconds,
              temp2_time_stamp.SubSeconds				
				  	);		
            /* test */
#endif
					  rx_user_uart_state = 0;
				  }
				  rxCounter++;
				  break;
				
			  default:		
				  break;
		  } // end switch (rx_user_uart_state)
	  } // end while ((rxCharRS485 = RS_485_getChar()) != -1)
	} // end for (rx_user_uart_state = 0;;)
} // end static void UserUartTask(void const *params)



#if 0
void RTCMonTask(void const *params)
{
  RTC_TimeTypeDef temp_time_stamp;
  RTC_DateTypeDef temp_date_stamp;

	uint8_t prev_sec;
	uint8_t rtc_mon_state;
	
	/* update GPIO_6 to reflect current second: 
      odd second: 1
     even second: 0
	*/
			
	for(rtc_mon_state = 0;;)
	{
	  HAL_RTC_GetTime(&hrtc,&temp_time_stamp,RTC_FORMAT_BIN);
	  HAL_RTC_GetDate(&hrtc,&temp_date_stamp,RTC_FORMAT_BIN);

		if ((rtc_mon_state == 0) ||(prev_sec != temp_time_stamp.Seconds))
	  {
	  	if (temp_time_stamp.Seconds & 0x01)
	  		GPIO_reset(GPIO_6);
	  	else
	  	  GPIO_set(GPIO_6);
		  prev_sec = temp_time_stamp.Seconds;
	  } // end if ((rtc_mon_state == 0) ||(prev_sec != temp_time_stamp.Seconds))
			
		if (rtc_mon_state == 0)
			rtc_mon_state = 1;
		//delay(1);
	}	// end for(;;)	
}
#endif

#ifdef SYNC_TEST
void RTC_Alarm_IRQHandler(void)
{
	RTC_TimeTypeDef temp_time_stamp;
  RTC_DateTypeDef temp_date_stamp;
	
	//printf("tick..\n");
	
  HAL_RTC_GetTime(&hrtc,&temp_time_stamp,RTC_FORMAT_BIN);
  HAL_RTC_GetDate(&hrtc,&temp_date_stamp,RTC_FORMAT_BIN);
	
  if (temp_time_stamp.Seconds & 0x01)
    GPIO_reset(GPIO_6);
	else
	  GPIO_set(GPIO_6);
}
#endif // #ifdef SYNC_TEST

static uint8_t readNetworkAddress()
{
	uint32_t gpio_pin;
	uint8_t i, address = 0;
	
	for (i=0, gpio_pin = networkAddrReadGPIOfirst;
       i<RS_485_NETWORK_ADDRESS_BIT_COUNT; 
	     i++, gpio_pin<<=1)
	{
		if (GPIO_read_pin(gpio_pin))
       address |= gpio_pin;	
	}	
	return address & RS_485_NETWORK_MASK; // address 00 thru 31
}

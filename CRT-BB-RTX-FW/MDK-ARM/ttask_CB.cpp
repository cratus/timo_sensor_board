#include "TIMO.h"
#include "main.h"
#include "cmsis_os.h"
#include "RS_485.h"
#include <time.h>

static RTC_TimeTypeDef temp_time_stamp;
static RTC_DateTypeDef temp_date_stamp;

/*
 * Terminal Task - Takes input command to do specific task
 * 
 * 
*/
static void 
TerminalTask(void const *params)
{
	int i;
  uint8_t prev_sec = -9;
	
	while(1) 
	{
		i = 0;
		int c;
		char str[256];
		printf("BB> ");
		do
		{
			c = fgetc(stdin);
			if(c=='\n')
				break;

			if(c!=-1)
			{
				str[i++] = c;
			}
			delay(10);
			
			
			HAL_RTC_GetTime(&hrtc,&temp_time_stamp,RTC_FORMAT_BIN);
			HAL_RTC_GetDate(&hrtc,&temp_date_stamp,RTC_FORMAT_BIN);

			if (prev_sec != temp_time_stamp.Seconds)
			{
				printf("%02d:%02d\n",
				temp_time_stamp.Minutes,					
				temp_time_stamp.Seconds);		
				prev_sec = temp_time_stamp.Seconds;
			}			
		}while(1);
		str[i]='\0';
	}
}

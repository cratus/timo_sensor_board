#include "TIMO.h"
#include "main.h"
#include "cmsis_os.h"
#include "RS_485.h"
#include <time.h>

/*
 *
 * User Uart Task
 *
 *
 */

static timoPacket 
  txPacket,
	rxPacket;
static uint8_t rxCounter;
static uint8_t *rxPkt;

static RTC_TimeTypeDef curr_time_stamp;
static RTC_DateTypeDef curr_date_stamp;
static uint8_t rx_user_uart_state;




void UserUartTask(void const *params)
{
  uint8_t addr;
	uint8_t *rx_pkt, i;
	uint32_t secs;
	char buf[30];
	
	osEvent evt;
	
	//sprintf(buf, "TIMO Coordinator Board\r\n");
	//USER_UART.println(buf);

	for (;;)
	{
    delay(20);

		//RS_485_sendAddressCharacter(0x2F); // Send Round-Trip Address character
		RS_485_sendAddressCharacter(0x4F); // Send Get Time Address character
		
		//putchar('.');

	} // end for(;;)
} // end static void UserUartTask(void const *params)


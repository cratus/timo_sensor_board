/*
$DESCRIPTION		: This file contains definitions for UART functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-14 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_IO_SHORTCUTS_H
#define __BLUEBRAIN_IO_SHORTCUTS_H

	#include "BlueBrain_UART.h"
	#include "BlueBrain_ADC.h"
	#include "BlueBrain_LED.h"
	#include "BlueBrain_I2C.h"
	#include "BlueBrain_SPI.h"
	#include "BlueBrain_LIS2DH.h"
	#include "BlueBrain_SI7050.h"
	
	#define BLE						BLE_UART :: getInstance()
	#define USER_UART			User_UART :: getInstance()
	#define DEBUG_UART		Debug_UART :: getInstance()
	#define USER_SPI			User_SPI :: getInstance()
	#define FLASH_SPI			Flash_SPI :: getInstance()
	#define SEN_I2C				Sensor_I2C :: getInstance()
	#define USER_I2C			I2C :: getInstance()
	#define LE						LED	:: getInstance()
	#define AS						LIS2DH :: getInstance()
	#define TS						SI7050 :: getInstance()
	
#endif /*__BLUEBRAIN_IO_SHORTCUTS_H */

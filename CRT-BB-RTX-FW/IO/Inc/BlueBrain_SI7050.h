/*
$DESCRIPTION		: This file contains definitions for SI7050 Temperature Sensor of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_SI7050_H
#define __BLUEBRAIN_SI7050_H


/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "singleton_template.h"
	 
// I2C Address for SI7050
#define SI7050_DEFAULT_ADDRESS 					0x81							// (0x40<<1 | 0x01)
#define MEASURE_TEMP										0xF3							// No hold Master
#define MEASURE_TEMP_HLD_MASTER					0xE3							// Hold Master
#define SI7050_RESET										0xFE
#define WRITE_USER1											0xE6
#define READ_USER1											0xE7
#define READ_ID1												0xFA0F
#define READ_ID2												0xFCC9
#define READ_FW_REV											0x84B8
	 
class SI7050 : public SingletonTemplate<SI7050>
{
		public:
						SI7050();
						bool init(void);
						uint8_t  getID(void);
						uint8_t readFWversion(void);
						float getTemperatureCenti(void);
						float getTemperatureFahren(void);
						
		private:
						uint8_t _address;	
						friend class SingletonTemplate<SI7050>;
		
						bool writeRegister(const uint8_t register_addr, const uint8_t value);
						bool writeRegisters(const uint8_t msb_register, const uint8_t msb_value, const uint8_t lsb_register, const uint8_t lsb_value);
						bool writeMaskedRegister(const uint8_t register_addr, const uint8_t mask, const bool value);
						uint8_t readRegister(const uint8_t register_addr);
						uint16_t readRegisters(const uint8_t msb_register, const uint8_t lsb_register);
						uint8_t readMaskedRegister(const uint8_t register_addr, const uint8_t mask);
};
		

#endif /*__BLUEBRAIN_SI7050_H */

/*
$DESCRIPTION		: This file contains definitions for SI7050 Temperature sensor functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

#include "BlueBrain_SI7050.h"

extern I2C_HandleTypeDef hi2c1;

SI7050::SI7050() 
{
    _address = SI7050_DEFAULT_ADDRESS;
}

bool
SI7050::init(void) 
{
    _address = SI7050_DEFAULT_ADDRESS; 
	
		uint8_t status = HAL_I2C_IsDeviceReady(&hi2c1,_address,10,100);
		if(status == HAL_OK) {
			return true;
		}
		else {
			return false;
		}
		
		// Already initialized in BlueBrain_init()
}

bool
SI7050::writeRegister(const uint8_t register_addr, const uint8_t value) 
{
		//send write call to sensor address
    //send register address to sensor
    //send value to register
    
		uint8_t data_to_send = value;
	
		uint8_t status = HAL_I2C_Mem_Write(&hi2c1, _address,(uint16_t)register_addr,1,&data_to_send,1,100);
		if(status == HAL_OK) {
			return true;
		}
		else {
			return false;   	//returns whether the write succeeded or failed
		}
}

uint8_t
SI7050 :: readRegister(const uint8_t register_addr) 
{
    //send write call to sensor address
    //send register address to sensor
    //send value to register
  
		uint8_t value = 0;
	
		uint8_t status = HAL_I2C_Mem_Read(&hi2c1, _address,(uint16_t)register_addr,1,&value,1,100);
		
		return value; //returns whether the write succeeded or failed
}

bool
SI7050::writeRegisters(const uint8_t msb_register, const uint8_t msb_value, const uint8_t lsb_register, const uint8_t lsb_value) 
{ 
    //send write call to sensor address
    //send register address to sensor
    //send value to register
    bool msb_bool, lsb_bool;
    msb_bool = writeRegister(msb_register, msb_value);
    lsb_bool = writeRegister(lsb_register, lsb_value);
    return (msb_bool | lsb_bool); //returns whether the write succeeded or failed
}

bool
SI7050::writeMaskedRegister(const uint8_t register_addr, const uint8_t mask, const bool value) 
{
    uint8_t data = readRegister(register_addr);
    uint8_t combo;
    if(value) {
        combo = (mask | data);
    } 
		else {
        combo = ((~mask) & data);
    }
    return (writeRegister(register_addr, combo));
}

uint16_t
SI7050::readRegisters(const uint8_t msb_register, const uint8_t lsb_register) 
{
    uint8_t msb = readRegister(msb_register);
    uint8_t lsb = readRegister(lsb_register);
    return ((((int16_t)msb) << 8) | lsb);
}

uint8_t
SI7050::readMaskedRegister(const uint8_t register_addr, const uint8_t mask) 
{
    uint8_t data = readRegister(register_addr);
    return (data & mask);
}

float 
SI7050 :: getTemperatureCenti(void)
{
		uint8_t value[2] = {0,0};
		uint8_t status;
		float lf_multFactor = 175.72 / (1<<16);
		status = HAL_I2C_Mem_Read(&hi2c1, _address,(uint16_t)MEASURE_TEMP_HLD_MASTER, I2C_MEMADD_SIZE_8BIT,value,2,100);
		
		uint16_t final_value = ((uint16_t)value[0]<<8) | (uint16_t)value[1] ;
		float temperature_data = (final_value * lf_multFactor) - 46.85;
		return temperature_data;      //returns Temperature value in degree centigrade
}

float 
SI7050 :: getTemperatureFahren(void)
{
		float temperature_data = getTemperatureCenti();
		temperature_data = (temperature_data*1.8) + 32;
		return temperature_data;  
}

uint8_t
SI7050 :: getID(void)
{
		uint8_t value;
		uint8_t	status = HAL_I2C_Mem_Read(&hi2c1, _address, READ_ID1,I2C_MEMADD_SIZE_16BIT,&value,1,100);
		return value;
}

uint8_t
SI7050 :: readFWversion(void)
{
		uint8_t value;
		uint8_t status = HAL_I2C_Mem_Read(&hi2c1, _address, READ_FW_REV,I2C_MEMADD_SIZE_16BIT,&value,1,100);
		return value;
}

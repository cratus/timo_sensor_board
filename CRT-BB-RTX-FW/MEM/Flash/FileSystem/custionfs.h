/* FILE: custionfs.h */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/

#if !defined( CUSTIONFS_H_26052009 )
#define CUSTIONFS_H_26052009

#define IONFS
#undef IONFS_SFILEMODE
#undef IONFS_STRICT_FAT
#undef IONFS_UNICODE
//#define IONFS_CPATH
#define IONFS_CDATA
#define IONFS_LOG
#undef IONFS_CHKDISK
//#define IONFS_WB
#define IONFS_FORBID_CHAR

#undef IONFS_DBG
#define IONFS_TRACE (0)
#undef IONFS_PL_T32
#undef IONFS_PL_WIN32

#define USE_STD_LIB

#define IONFS_ALIGN_NONE (1<<1)
#define IONFS_ALIGN_DATA_SECT (1<<2)
#define IONFS_ALIGN_ROOT_DIR (1<<3)

#define IONFS_ALIGN (IONFS_ALIGN_DATA_SECT)

#define IONFS_OS_NONE (1<<1)
#define IONFS_OS_NUCLEUS (1<<2)
#define IONFS_OS_REX (1<<3)
#define IONFS_OS_ionOS (1<<4)
#define IONFS_OS_uCOS_II (1<<5)
#define IONFS_OS_WIN32 (1<<6)
#define IONFS_OS_LINUX (1<<7)
#define IONFS_OS_RTX_CRATUS (1<<8)

#define IONFS_OS (IONFS_OS_RTX_CRATUS)

#define IONFS_DEV_OneNAND (1<<1)
#define IONFS_DEV_NOR (1<<2)
#define IONFS_DEV_MMC (1<<3)
#define IONFS_DEV_RAM (1<<4)
#define IONFS_DEV_NAND (1<<5)
#define IONFS_DEV_ORNAND (1<<6)
#define IONFS_DEV_yourDEV (1<<7)

#define IONFS_DEVS (IONFS_DEV_NOR)

#define IONFS_BD_L2P (1<<1)
#define IONFS_BD_RAM (1<<2)
#define IONFS_BD_MMC (1<<3)
#define IONFS_BD_SPANSION (1<<4)
#define IONFS_BD_yourBD (1<<5)

#define IONFS_BD (IONFS_BD_SPANSION )

#define IONFS_DEVICE_NUM (1)	       /*2BDSOL* change to 2 for 2 BDs */
#define IONFS_VOLUME_NUM (1)           /*2BDSOL* change to 2 for 2 BDs */

#define FAT_HEAP_SIZE (2*1024)		//(8*1024)

#define LIM_CACHE_NUM (4)	//(64)
#define FAT_CACHE_NUM (2)	//(32)
#define PATH_CACHE_NUM (4)

#define SECTOR_NUM_PER_CACHE (1)

#define FILE_WB_CNT 1
#define FILE_WB_SIZE (16*1024)

#define FAT_MAX_ODIR 1	//10
#define FAT_MAX_FILE 2	//(20)
#define FAT_MAX_OFILE (FAT_MAX_FILE+(FAT_MAX_FILE/2))

#undef IONFS_FS_MALLOC

#if defined( IONFS_FS_MALLOC )
#define OSD_HEAP_SIZE (8*1024)
#endif

#define READ_CRITICAL_SECTION

#endif /**/


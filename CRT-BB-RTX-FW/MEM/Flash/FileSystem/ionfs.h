/* FILE: ionfs.h */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


#if !defined( IONFS_H_28112005 )
#define IONFS_H_28112005


#if defined (__cplusplus )
extern "C" {
#endif

#include "custionfs.h"
#include "stdint.h"

/*---------------------------- Define base type definition: -----------------*/
#if !defined( IONFS_TYPE_DEFINE ) && !defined( IONOS_TYPE_DEFINE )
#define IONFS_TYPE_DEFINE

#if defined( IONFS_INT_DEFINED )
typedef signed char int8_t;
typedef unsigned char uint8_t;

typedef signed short int int16_t;
typedef unsigned short int uint16_t;

typedef signed long int  int32_t;
typedef unsigned long int uint32_t;


#define IONFS_INT_DEFINED
#endif


#if !defined( IONFS_TCHAR_DEFINED )
#define _TCHAR_DEFINED     /* for WIN32 */

#if defined( IONFS_UNICODE )
#undef _TSTR
#undef _TC
#undef _ATC
#define _TSTR(s) L###s
#define __TC(str) ((char_t *) L##str)
#define _ATC(str) L##str
typedef unsigned short int char_t;
#else
#undef _TSTR
#undef _TC
#undef _ATC
#define _TSTR(s) #s
#define __TC(str) ((char_t *) str)
#define _ATC(str) str
typedef unsigned char char_t;
#endif
#define _TC(str) __TC(str)

#define IONFS_TCHAR_DEFINED
#endif


#if !defined( IONFS_BOOL_DEFINED )
typedef unsigned char bool_t;
#define IONFS_BOOL_DEFINED
#endif


#if !defined( IONFS_STATUS_DEFINED )
typedef uint16_t status_t;
#define IONFS_STATUS_DEFINED
#endif


#if !defined( IONFS_MODE_DEFINED )
typedef uint32_t mod_t;
#define IONFS_MODE_DEFINED
#endif


#if !defined( IONFS_OFFS_DEFINED )
typedef int32_t offs_t;
#define IONFS_OFFS_DEFINED
#endif


#if !defined( IONFS_SIZE_DEFINED )
typedef uint32_t siz_t;
#define IONFS_SIZE_DEFINED
#endif


#if !defined( IONFS_SSIZE_DEFINED )
typedef int32_t ssiz_t;
#define IONFS_SSIZE_DEFINED
#endif


#if !defined(IONFS_CONSOLE)
typedef void (*console_t)( char );
typedef int (*cprint_t)( console_t cs, const char *format, ... );
#define IONFS_CONSOLE
#endif


#if !defined(ION_offsetof)
#define ION_offsetof(s,m)  (int32_t)&(((s *)0)->m)
#endif


#define ION_UINT8_MAX 0xFF
#define ION_UINT16_MAX 0xFFFF
#define ION_UINT32_MAX 0xFFFFFFFF


#if !defined( NULL )
#define NULL ((void*)0)
#endif


#undef TRUE
#undef FALSE
#define TRUE 1
#define FALSE 0


#if !defined( __cplusplus )
   #if !defined( bool )
      #define bool int
      #define true 1
      #define false 0
   #endif
#endif


#if !defined( IONFS_UNUSED ) && !defined( UNUSED )
//#define UNUSED(x) (void)(x)
#define IONFS_UNUSED
#endif


#if !defined( IONFS_INLINE ) && !defined( INLINE )
/*__inline*/
#if defined( IONFS_DBG )
#define INLINE
#else
#define INLINE __inline
#endif
#define IONFS_INLINE
#endif
#endif

#if !defined( ionfs_local )
#define ionfs_local
#endif


#define IONFS_FILENAME_MAX 255




typedef enum {
   IONFS_FAT16 = 16,
   IONFS_FAT32 = 32

} fstype_t;


typedef struct dirent_s {
   /* Name of the directory entry */
   char_t d_name[IONFS_FILENAME_MAX+1/*null char*/];

} dirent_t;


typedef struct DIR_s {
   int32_t fd;             /* The file descriptor */
   uint32_t attr,          /* The attribute of directory */
            ctime,         /* Time of creation of the directory */
            atime,         /* Time of last access of the directory */
            wtime,         /* Time of last writing of the directory */
            filesize;      /* Size of the directory in bytes */
#if defined( IONFS_SFILEMODE )
   uint32_t filemode;
#endif
   dirent_t dirent;

} DIR_t;


typedef struct stat_s {
   uint16_t dev,           /* Drive number of the disk containing the file (same as rdev) */
            i_no,          /* i-node number (not used on DOS) */
            mode,          /* Bit mask for file-mode information */
            nlink;         /* Always 1 on non-NTFS file systems */
   int32_t uid,            /* Numeric identifier of user who owns file (UNIX-specific) */
           gid;            /* Numeric identifier of group that owns file (UNIX-specific) */
   uint16_t rdev;          /* Drive number of the disk containing the file (same as dev) */
   uint32_t size,          /* Size of the file in bytes */
            atime,         /* Time of last access of file */
            mtime,         /* Time of last modification of file */
            ctime;         /* Time of creation of file */
#if defined( IONFS_SFILEMODE )
   uint32_t filemode;
#endif

} stat_t;


typedef struct statdir_s {
   uint32_t files,         /* count of total files in directory */
            size,          /* size of total files in directory */
            alloc_size,    /* allocated size of total files in directory */
            atime,         /* Time of last access of file */
            mtime,         /* Time of last modification of file */
            ctime;         /* Time of creation of file */

} statdir_t;


typedef struct statfs_s {
   int32_t blk_size,       /* fundamental file system block size */
           io_size,        /* optimal transfer block size */
           blocks,         /* total data blocks in file system */
           free_blks,      /* free blocks in fs */
           fs_id,          /* id of file system */
           type;           /* type of filesystem */

} statfs_t;


typedef struct partinfo_s {
   int32_t start_sect,     /* start sector */
           end_sect,       /* end sector */
           sectors,        /* count of total sectors */
           bytes_per_sect; /* byte per sector */

} partinfo_t;        /* partition info */


typedef struct ioarg_s {
   uint32_t sect_no,
            sect_cnt;
   uint8_t *buf;

} iorw_t;


typedef struct mcb_s {
   uint32_t words[32];

} mcb_t;


typedef struct fsver_s{
   uint8_t name[32];
   uint32_t major,
            minor,
            stable;
}fsver_t;


/* Error numbers */
typedef enum {
    IONFS_OK = 0,

    IONFS_EACCES = -1,        /* Search permission is denied for a directory in a file's path prefix. */
    IONFS_EBUSY = -2,         /* The device or resource is in use. */
    IONFS_EEXIST = -3,        /* The named file already exists. */
    IONFS_EINVAL = -4,        /* Invalid argument. */
    IONFS_ENOENT = -5,        /* No such file or directory */
    IONFS_EIO = -6,           /* I/O error */
    IONFS_EISDIR = -7,        /* Attempt to open a directory for writing or to rename a file to be a directory. */
    IONFS_ENAMETOOLONG = -8,  /* Length of a filename string exceeds PATH_MAX and _POSIX_NO_TRUNC is in effect. */
    IONFS_EPATH = -9,         /* invalid path */
    IONFS_ENFILE = -10,       /* Too many files are currently open in the system. */
    IONFS_ENDIR = -11,        /* Too many directories are currently open in the system. */
    IONFS_ENODEV = -12,       /* No such device. Attempt to perform an inappropriate function to a device. */
    IONFS_ENOMEM = -13,       /* No memory available. */
    IONFS_ENOSPC = -14,       /* No space left on disk. */
    IONFS_ENOTDIR = -15,      /* A component of the specified pathname was not a directory when a directory was expected. */
    IONFS_ENOTEMPTY = -16,    /* Attempt to delete or rename a non-empty directory. */
    IONFS_EPERM = -17,        /* Operation is not permitted. */
    IONFS_EROFS = -18,        /* Read-only file system. */
    IONFS_EXDEV = -19,        /* Attempt to link a file to another file system. in rename() (volume is different) */
    IONFS_EBADF = -20,        /* Invalid file descriptor. */
    IONFS_ESPIPE = -21,       /* Illegal seek */
    IONFS_EATTACH = -22,      /* attached mass-storage */
    IONFS_EEJECT = -23,       /* Media was ejected. */
    IONFS_ECFS = -24,         /* File system was corruped. */
    IONFS_ECFAT = -25,        /* FAT table was corrupted. */
    IONFS_EAFAT = -26,        /* FAT table invalid access. */
    IONFS_EFCACHE = -27,      /* FAT cache problem */
    IONFS_ELCACHE = -28,      /* LIM cache problem */
    IONFS_EEOS = -29,         /* The searching reached to the end of entry. use only internally */
    IONFS_EOUTOF = -30,       /* out of access */
    IONFS_ENOINIT = -31,      /* File sytem was not initialized */
    IONFS_ENOMNT = -32,       /* File system was not mounted */
    IONFS_ENOFMT = -33,       /* File system was not formatted */
    IONFS_EMBR = -34,         /* MBR was not formatted */
    IONFS_EPORT = -35,        /* File system become miss porting */
    IONFS_EOSD = -36,         /* OS problerms */
    IONFS_EOVERW = -37,       /* Internel datas were overwritten */
    IONFS_EWBF = -38,         /* Write buffer flush fail */
    IONFS_EPLO = -39,         /* Power Loss occurred(Only uses internally) */
    IONFS_EUNKNOWN = -99,

    IONFS_ERRNO = -100
} errno_t;




/* ionfs_format()'s flag */
#define IONFS_FORMAT_FAST (1<<0)
#define IONFS_FORMAT_SCAN (1<<1)




/* Non-ANSI names for POSIX */

#define ION_O_RDONLY  0x0000   /* open for reading only */
#define ION_O_WRONLY  0x0001   /* open for writing only */
#define ION_O_RDWR    0x0002   /* open for reading and writing */
#define ION_O_APPEND  0x0008   /* writes done at eof */
#define ION_O_CREAT   0x0100   /* create and open file */
#define ION_O_TRUNC   0x0200   /* open and truncate */
#define ION_O_EXCL    0x0400   /* open only if file doesn't already exist */




/* permissions */
#undef ION_S_IRWXU
#undef ION_S_IRUSR
#undef ION_S_IWUSR
#undef ION_S_IXUSR
#define ION_S_IRWXU 00700
#define ION_S_IRUSR 00400
#define ION_S_IWUSR 00200
#define ION_S_IXUSR 00100
#undef ION_S_IRWXG
#undef ION_S_IRGRP
#undef ION_S_IWGRP
#undef ION_S_IXGRP
#define ION_S_IRWXG 00070
#define ION_S_IRGRP 00040
#define ION_S_IWGRP 00020
#define ION_S_IXGRP 00010
#undef ION_S_IRWXO
#undef ION_S_IROTH
#undef ION_S_IWOTH
#undef ION_S_IXOTH
#define ION_S_IRWXO 00007
#define ION_S_IROTH 00004
#define ION_S_IWOTH 00002
#define ION_S_IXOTH 00001




/* File position */
#undef ION_SEEK_SET
#undef ION_SEEK_CUR
#undef ION_SEEK_END
#define ION_SEEK_SET  0        /* Indicates the beginning of the file. */
#define ION_SEEK_CUR  1        /* Indicates the current of the file. */
#define ION_SEEK_END  2        /* Indicates the end position. */




/* File permittion */
#undef ION_R_OK
#undef ION_W_OK
#undef ION_X_OK
#undef ION_F_OK
#define ION_R_OK  4            /* for read */
#define ION_W_OK  2            /* for write */
#define ION_X_OK  1            /* for execute */
#define ION_F_OK  0            /* for existence */




/* File attribute */
#define FA_RDONLY    0x01  /* a file for reading only */
#define FA_HIDDEN    0x02  /* a hidden file */
#define FA_SYSTEM    0x04  /* a system file */
#define FA_VOLUME    0x08  /* a volume */
#define FA_DIRECTORY 0x10  /* a directory */
#define FA_ARCHIVE   0x20  /* a archive file */




/* Ioctl function */
#define IO_MS_ATTACH    (1<<0)
#define IO_MS_DETACH    (1<<1)
#define IO_DEV_ATTACH   (1<<2)
#define IO_DEV_DETACH   (1<<3)
#define IO_PART_INFO    (1<<4)  /* partition info */
#define IO_OP           (1<<5)
#define IO_READ         (IO_OP | 1<<6)
#define IO_WRITE        (IO_OP | 1<<7)
#define IO_ERASE        (IO_OP | 1<<8)




/* Function map */
#if 1

#define ionFS_zero_init          fsm_zinit_fs
#define ionFS_init               fsm_init_fs
#define ionFS_terminate          fsm_terminate
#define ionFS_format             fsm_format
#define ionFS_mount              fsm_mount
#define ionFS_umount             fsm_umount
#define ionFS_sync               fsm_sync
#define ionFS_statfs             fsm_statfs
#define ionFS_ioctl              fsm_ioctl
#define ionFS_mkdir              fsm_mkdir
#define ionFS_rmdir              fsm_rmdir
#define ionFS_opendir            fsm_opendir
#define ionFS_readdir            fsm_readdir
#define ionFS_rewinddir          fsm_rewinddir
#define ionFS_closedir           fsm_closedir
#define ionFS_cleandir           fsm_cleandir
#define ionFS_statdir            fsm_statdir
#define ionFS_access             fsm_access
#define ionFS_creat              fsm_creat
#define ionFS_open               fsm_fopen
#define ionFS_read               fsm_read
#define ionFS_write              fsm_write
#define ionFS_lseek              fsm_lseek
#define ionFS_fsync              fsm_fsync
#define ionFS_close              fsm_fclose
#define ionFS_closeall           fsm_closeall
#define ionFS_unlink             fsm_unlink
#define ionFS_truncate           fsm_truncate
#define ionFS_tell               fsm_tell
#define ionFS_rename             fsm_rename
#define ionFS_stat               fsm_stat
#define ionFS_fstat              fsm_fstat
#define ionFS_getattr            fsm_getattr
#define ionFS_fgetattr           fsm_fgetattr
#define ionFS_setattr            fsm_setattr
#define ionFS_fsetattr           fsm_fsetattr
#define ionFS_set_safe_mode      os_set_safe_mode
#define ionFS_get_sectors        fsm_get_sectors
#define ionFS_get_devicetype     fsm_get_devicetype
#define ionFS_get_version        fsm_get_version
#if defined( IONFS_FS_MALLOC )
#define ionFS_init_heap          os_init_heap
#define ionFS_delete_heap        os_terminate_heap
#define ionFS_malloc             os_malloc
#define ionFS_free               os_free
#endif
#define ionFS_set_errno          os_set_errno
#define ionFS_get_errno          os_get_errno

#endif


/* ------------------------------- Global Funtions ------------------------------- */
void fsm_zinit_fs( void );
int32_t fsm_init_fs( void );
int32_t fsm_terminate( void );
int32_t fsm_format( const char_t *vol, uint32_t dev_id, uint32_t part_no, uint32_t start_sec,
                    uint32_t cnt, char_t *fs_type, uint32_t opt );
int32_t fsm_mount( const char_t *vol, uint32_t dev_id, uint32_t part_no, uint32_t opt  );
int32_t fsm_umount( const char_t *vol, uint32_t opt );
int32_t fsm_sync( const char_t *vol );
int32_t fsm_statfs( const char_t *vol, statfs_t *statbuf );
int32_t fsm_ioctl( const char_t *vol, uint32_t func, void *param );
int32_t fsm_mkdir( const char_t *path, mod_t mode/*permission*/ );
int32_t fsm_rmdir( const char_t *path );
DIR_t* fsm_opendir( const char_t *path );
dirent_t *fsm_readdir( DIR_t *de );
int32_t fsm_rewinddir( DIR_t *debuf );
int32_t fsm_closedir( DIR_t *debuf );
int32_t fsm_cleandir( const char_t *path );
int32_t fsm_statdir( const char_t *path, statdir_t *statbuf );
int32_t fsm_access( const char_t *path, int32_t amode );
int32_t fsm_creat( const char_t *path, mod_t mode );
int32_t fsm_fopen( const char_t *path, uint32_t flag, ... /* mod_t mode */ );
ssiz_t fsm_read( int32_t fd, void *buf, siz_t bytes );
ssiz_t fsm_write( int32_t fd, const void *buf, siz_t bytes );
offs_t fsm_lseek( int32_t fd, offs_t offset, int32_t whence );
int32_t fsm_fsync( int32_t fd  );
int32_t fsm_fclose( int32_t fd );
int32_t fsm_closeall( const char_t *vol );
int32_t fsm_unlink( const char_t *path );
int32_t fsm_truncate( int32_t fd, siz_t new_size );
int32_t fsm_tell( int32_t fd );
int32_t fsm_rename( const char_t *oldpath, const char_t *newpath );
int32_t fsm_stat( const char_t *path, stat_t *statbuf );
int32_t fsm_fstat( int32_t fd, stat_t *statbuf );
int32_t fsm_getattr( const char_t *path, uint32_t *attrbuf );
int32_t fsm_fgetattr( int32_t fd, uint32_t *attrbuf );
int32_t fsm_setattr( const char_t *path, uint32_t attr );
int32_t fsm_fsetattr( int32_t fd, uint32_t attr );
bool_t fsm_set_safe_mode( bool_t issafe );
bool_t os_set_safe_mode( bool_t is_safe );
int32_t fsm_get_sectors( int32_t dev_id );
int32_t fsm_get_devicetype( int32_t dev_id, char_t * name );
const fsver_t *fsm_get_version( void );
#if defined( IONFS_FS_MALLOC )
void os_init_heap( void );
void os_terminate_heap( void );
void *os_malloc( uint32_t size );
void os_free( void *block );
#endif
int32_t os_set_errno( int32_t err_no );
int32_t os_get_errno( void );




#if defined (__cplusplus )
}
#endif

#endif


/*----------------------------------------------------------------------------
 END OF FILE
----------------------------------------------------------------------------*/

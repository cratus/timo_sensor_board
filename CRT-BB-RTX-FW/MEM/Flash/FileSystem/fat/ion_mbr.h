/* FILE: ion_mbr.h */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


#if !defined( MBR_H_25112005 )
#define MBR_H_25112005

/*-----------------------------------------------------------------------------
 INCLUDE HEADER FILES
-----------------------------------------------------------------------------*/

#include "../ionfs.h"
#include "../lim/ion_lim.h"




/*-----------------------------------------------------------------------------
 DEFINE DEFINITIONS
-----------------------------------------------------------------------------*/

/* The following value defined is written in the MBR area. */
#define MBR_LBA_CYLINDER  1023
#define MBR_LBA_HEAD  254
#define MBR_LBA_SECTOR  63




/*-----------------------------------------------------------------------------
 DEFINE STRUCTURES
-----------------------------------------------------------------------------*/

/* The enumuration value for the type of file system. */
typedef enum mbr_partition_type_e {
   eDOS_FAT16_L32 = 0x04,
   eDOS_FAT16_GE32 = 0x06,
   eWIN95_FAT32 = 0x0B

} mbr_partition_type_t;


/* Data structure on the MBR(Master Boot Record). */
typedef struct mbr_partition_s {
   uint8_t boot_ind;       /* boot indicator */
   uint8_t start_head;     /* beginning sector head number */
   uint8_t start_sect_cyl; /* beginning sector (2 high bits of cylinder) */
   uint8_t start_cyl;      /* beginning cylinder (low order bits of cylinder)*/
   uint8_t sys_ind;        /* partition type */
   uint8_t end_head;       /* ending sector head number */
   uint8_t end_sect_cyl;   /* ending sector (2 high bits of cylinder) */
   uint8_t end_cyl;        /* ending cylinder */
   uint32_t start_sect;    /* number of sectors preceding the partition */
   uint32_t totsect_cnt;   /* number of sectors in the partition */

} mbr_partition_t;




/*-----------------------------------------------------------------------------
 DECLARE FUNTION PROTO-TYPE
-----------------------------------------------------------------------------*/

int32_t mbr_creat_partition( uint32_t dev_id, uint32_t part_no, uint32_t start_cyl, uint32_t cnt, const char_t *fs_type );
int32_t mbr_load_partition( uint32_t dev_id, uint32_t part_no, lim_volinfo_t *lvi );

#endif

/*----------------------------------------------------------------------------
 END OF FILE
----------------------------------------------------------------------------*/


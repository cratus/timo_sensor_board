/* FILE: ion_pim.c */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


/*-----------------------------------------------------------------------------
 INCLUDE HEADER FILES
-----------------------------------------------------------------------------*/

#include "../lim/ion_lim.h"
#include "ion_pim.h"
#if ( IONFS_BD & IONFS_BD_L2P )
#include "ion/ion_pim_ion.h"
#endif
#if ( IONFS_BD & IONFS_BD_MMC )
#include "mmc/ion_pim_mmc.h"
#endif
#if ( IONFS_BD & IONFS_BD_RAM )
#include "ram/ion_pim_ram.h"
#endif
#if ( IONFS_BD & IONFS_BD_SPANSION )
#include "spansion/ion_pim_spansion.h"
#endif
#include "../fat/ion_mbr.h"
#include "../fat/ion_path.h"




/*-----------------------------------------------------------------------------
 DEFINE DATA TYPE & STRUCTURES
-----------------------------------------------------------------------------*/

/* Define setup function in each device. */

typedef int32_t (* pim_devsetupcallback_t)(pim_devinfo_t *, char_t *);


typedef struct pim_setup_s
{
   char_t * dev_name;
   pim_devsetupcallback_t dev_setup_callback;

} pim_setup_t;




static pim_setup_t dev_setup[] = {
   #if ( IONFS_DEVS & IONFS_DEV_ORNAND)
   {_TC("ornand"), pim_setup_span },
   #endif
   #if ( IONFS_DEVS & (IONFS_DEV_OneNAND | IONFS_DEV_NAND) )
   {_TC("nand"), pim_setup_ion},
   #endif
   #if ( IONFS_DEVS & IONFS_DEV_NOR )
   {_TC("nor"), pim_setup_span},
   #endif
   #if ( IONFS_DEVS & IONFS_DEV_MMC )
   {_TC("mmc"), pim_setup_mmc},
   #endif
   #if ( IONFS_DEVS & IONFS_DEV_RAM )
   {_TC("ramnand"), pim_setup_ram},
   {_TC("ramnor"), pim_setup_ram},
   #endif
   {NULL,NULL}
};

pim_devinfo_t pim_dev[IONFS_DEVICE_NUM];

bool_t pim_dev_inited[IONFS_DEVICE_NUM] = {false,};




/*-----------------------------------------------------------------------------
 DEFINE FUNCTIONS
-----------------------------------------------------------------------------*/

/*
 Name: pim_zinit_pim
 Desc: Zero initialize all data.
 Params: None.
 Returns: None.
 Caveats: None.
*/

void pim_zinit_pim( void )
{
   ionFS_memset( &pim_dev, 0, sizeof(pim_dev) );
   ionFS_memset( &pim_dev_inited, 0, sizeof(pim_dev_inited) );
}




/*
 Name: pim_init
 Desc: Initialize all devices.
 Params: None.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: This function check whether the setup function in each device exists
          or not. If setup function doesn't exist, it returns IONFS_EPORT error.
*/

int32_t pim_init( void )
{
   pim_setup_t *setup = &dev_setup[0];
   pim_devinfo_t *pdi = &pim_dev[0];
   uint32_t i;


   for ( i = 0; i < IONFS_DEVICE_NUM; i++ ) {
      if ( NULL == setup[i].dev_setup_callback )
         return os_set_errno( IONFS_EPORT );

      pdi[i].opened = false;
   }

   return IONFS_OK;
}




/*
 Name: pim_open
 Desc: Open a specific device.
 Params:
   - dev_id: Device's id to be opened.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: This function calls the setup function. The setup function in each
          device defines functions about each operation. If setup function
          doesn't exist, it returns IONFS_EPORT error.
*/

int32_t pim_open( int32_t dev_id )
{
   pim_setup_t setup = dev_setup[dev_id];
   pim_devinfo_t *pdi = &pim_dev[dev_id];
   int32_t rtn;


   if ( NULL == setup.dev_setup_callback || NULL == pdi )
      return os_set_errno( IONFS_EPORT );

   if ( true == pdi->opened ) {
      if ( ePIM_Removable & pdi->dev_flag ) {
         if ( 0 > pim_close( dev_id ) )
            return os_set_errno( IONFS_EIO );
      }
      else
         return IONFS_OK;
   }

   rtn = setup.dev_setup_callback( pdi, setup.dev_name );
   if ( 0 > rtn ) return os_set_errno( rtn );

   rtn = pdi->op.ioctl( dev_id, eIOCTL_init, NULL );
   if ( 0 > rtn ) return os_set_errno( rtn );

   rtn = pdi->op.ioctl( dev_id, eIOCTL_open, pdi );
   if ( 0 > rtn ) return os_set_errno( rtn );

   pdi->opened = true;

   return IONFS_OK;
}




/*
 Name: pim_close
 Desc: close a specific device.
 Params:
   - dev_id: Device's id to be closed.
 Returns:
   int32_t  0(=IONFS_OK) always.
 Caveats: None.
*/

int32_t pim_close( int32_t dev_id )
{
   pim_devinfo_t *pdi = &pim_dev[dev_id];


   if ( true == pdi->opened ) {
      pdi->op.ioctl( dev_id, eIOCTL_close, NULL );
      pdi->opened = false;
   }

   return IONFS_OK;
}




/*
 Name: pim_terminate
 Desc: Terminate all devices.
 Params: None.
 Returns:
   int32_t  0(=IONFS_OK) always.
 Caveats: After this function is called, the file system can't be used anymore.
*/

int32_t pim_terminate( void )
{
   pim_devinfo_t *pdi = &pim_dev[0];
   int32_t i;


   for ( i = 0; i < IONFS_DEVICE_NUM; i++ ) {
      if ( true == pdi[i].opened ) {
         pdi[i].op.ioctl( i, eIOCTL_close, NULL );
         pdi[i].op.ioctl( i, eIOCTL_terminate, NULL );
         pdi[i].opened = false;
      }
   }

   return IONFS_OK;
}




/*
 Name: pim_read_sector
 Desc: Read data from a sector in a specific device.
 Params:
   - dev_id: An ID of device.
   - sect_no: The sector number to be read.
   - buf: Pointer to a buffer in which the bytes read are placed.
   - count: The number of bytes to be read.
 Returns:
   int32_t  >0 on success.
            <0 on fail.
 Caveats: This function does sector-read function registered for device.
*/

int32_t pim_read_sector( int32_t dev_id, uint32_t sect_no, uint8_t *buf,
                         uint32_t count )
{
   /* Read the operation's pointer of device */
   pim_devop_t *pdo = &pim_dev[dev_id].op;

   /* Call the Device read function */
   return pdo->read_sector( dev_id, sect_no, buf, count );
}




/*
 Name: pim_read_at_sector
 Desc: Read data from a specific offset of sector in a specific device.
 Params:
   - dev_id: An ID of device.
   - sect_no: The sector number to be read.
   - offs: The amount byte offset in the sector to be read.
   - len: The number of bytes to be read.
   - buf: Pointer to a buffer in which the bytes read are placed.
 Returns:
   int32_t  >0 on success.
            <0 on fail.
 Caveats: This function does sector-read-at-offset function registered for
          device.
*/

int32_t pim_read_at_sector( int32_t dev_id, uint32_t sect_no, uint32_t offs,
                            uint32_t len, uint8_t *buf )
{
   /* Read the operation's pointer of device */
   pim_devop_t *pdo = &pim_dev[dev_id].op;

   /* Read data from a specific offset of sector */
   return pdo->read_at_sector( dev_id, sect_no, offs, len, buf );
}




/*
 Name: pim_write_sector
 Desc: Write data to a sector in a specific device.
 Params:
   - dev_id: An ID of device.
   - sect_no: The sector number which data is written to.
   - buf: Pointer to a buffer containing the data to be written.
   - count: The size in bytes of the data to be written.
 Returns:
   int32_t  >0 on success.
            <0 on fail.
 Caveats: This function does sector-write function registered for device.
*/

int32_t pim_write_sector( int32_t dev_id, uint32_t sect_no, uint8_t *buf,
                          uint32_t count )
{
   /* Read the operation's pointer of device */
   pim_devop_t *pdo = &pim_dev[dev_id].op;

   /* Call the Device write function */
   return pdo->write_sector( dev_id, sect_no, buf, count );
}




/*
 Name: pim_erase_sector
 Desc: Erase a sector in a specific device.
 Params:
   - dev_id: An ID of device.
   - sect_no: Sector number to be erased.
   - count: A number of sectors to be erased.
 Returns:
   int32_t  >0 on success.
            <0 on fail.
 Caveats: This function does sector-erase function registered for device.
*/

int32_t pim_erase_sector( int32_t dev_id, uint32_t sect_no, uint32_t count )
{
   pim_devop_t *pdo = &pim_dev[dev_id].op;

   /* Call the PIM's erase function */
   return pdo->erase_sector( dev_id, sect_no, count );
}




/*
 Name: pim_get_sectors
 Desc: Get the number of sectors in a specific device.
 Params:
   - dev_id: An ID of device.
 Returns:
   int32_t  >0 on success. The return value is the number of sectors.
            <0 on fail.
 Caveats: If the device didn't open, this function returns IONFS_ENOINIT error.
          This function returns the number of sectors in the device except MBR.
*/

int32_t pim_get_sectors( int32_t dev_id )
{
   pim_devinfo_t *pdi = &pim_dev[dev_id];

   /* Check the state of PIM's initialization */
   if ( !pdi->opened )
      return IONFS_ENOINIT;

   /* return the initialized-sector's count  */
   return pim_dev[dev_id].totsect_cnt;
}




/*
 Name: pim_get_devicetype
 Desc: Get name of specific device.
 Params:
   - dev_id: An ID of device.
   - name: Device type name.
 Returns:
   int32_t  >0 on success. The return value is the number of sectors.
            <0 on fail.
 Caveats: If the device didn't open, this function returns IONFS_ENOINIT error.
*/

int32_t pim_get_devicetype( int32_t dev_id, char_t * name )
{
   pim_devinfo_t *pdi = &pim_dev[dev_id];

   /* Check the PIM's initialize state */
   if ( !pdi->opened )
      return IONFS_ENOINIT;
   ionFS_t_lstrcpy(name, pdi->dev_name);

   return IONFS_OK;
}

/*-----------------------------------------------------------------------------
 END OF FILE
-----------------------------------------------------------------------------*/


/* FILE: ion_osd.c */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


/*-----------------------------------------------------------------------------
 INCLUDE HEADER FILES
-----------------------------------------------------------------------------*/

#include "ion_osd.h"
#include <stdio.h>
#include <stdarg.h>


#if ( IONFS_OS & IONFS_OS_NONE )
/* nothing to do */
#elif ( IONFS_OS & IONFS_OS_WIN32 )
#include <windows.h>
CRITICAL_SECTION ionfs_cs;
#define entercriticalsection(x)       EnterCriticalSection((CRITICAL_SECTION*)x)
#define leavecriticalsection(x)       LeaveCriticalSection((CRITICAL_SECTION*)x)
#elif ( IONFS_OS & IONFS_OS_LINUX )
#include <pthread.h>
#include <time.h>
#endif



#if defined( WIN32 )
#include <assert.h>
#include <stdlib.h>
#endif

#if defined( TLC_PLATFORM ) && defined( TARGET ) /* Target of Agere System. */
#include "../global/timedate.h"
#endif

#if ( IONFS_OS & IONFS_OS_NUCLEUS )
#include "nucleus.h"
#elif ( IONFS_OS & IONFS_OS_ionOS )
#include "ionos.h"
#elif ( IONFS_OS & IONFS_OS_RTX_CRATUS )
#include "cmsis_os.h"
#endif

#include <string.h>
#if defined( IONFS_FS_MALLOC )
#include "../util/ion_malloc.h"
#endif

#if ( IONFS_OS & IONFS_OS_NONE )
typedef uint32_t ionfs_sem_t;
#endif


/*-----------------------------------------------------------------------------
 DEFINE GLOBAL VARIABLES
-----------------------------------------------------------------------------*/

/* Flag for OS safe-mode. */
#if ( IONFS_OS & IONFS_OS_NONE )
static bool_t os_safe_mode = true;
#else
static bool_t os_safe_mode;
#endif



/* Heap control block. */
#if defined( IONFS_FS_MALLOC )
static bool_t os_heap_inited;
static mcb_t os_fs_hcb;

static uint32_t os_fs_heap[OSD_HEAP_SIZE/sizeof(uint32_t)];
#endif


#define BASE_NAME "ionfs_"
#define IDX_POS sizeof(BASE_NAME-1)

#if ( IONFS_OS & IONFS_OS_NONE )
static bool_t os_sm_inited;
ionfs_sem_t os_fs_sm[SM_MAX];
void *os_fs_sm_ptr[SM_MAX];
#elif (IONFS_OS & IONFS_OS_RTX_CRATUS)
/* Variables related to semaphore */
static bool_t os_sm_inited;
osSemaphoreId os_fs_sm[SM_MAX];
osSemaphoreDef(os_fs_sm[SM_MAX]);
void *os_fs_sm_ptr[SM_MAX];
//void *os_fs_sm_id_ptr[SM_MAX];
#endif




/* error value */
errno_t os_errno;




/*-----------------------------------------------------------------------------
 DEFINE FUNCTIONS
-----------------------------------------------------------------------------*/

/*
 Name: os_zinit_osd
 Desc: Zero initialize all data.
 Params: None.
 Returns: None.
 Caveats: None.
*/

void os_zinit_osd( void )
{
   memset( &os_safe_mode, 0, sizeof(os_safe_mode) );
   memset( &os_sm_inited, 0, sizeof(os_sm_inited) );
   memset( &os_fs_sm, 0, sizeof(os_fs_sm) );
   memset( &os_fs_sm_ptr, 0, sizeof(os_fs_sm_ptr) );
}




#if defined( IONFS_FS_MALLOC )
/*
 Name: os_init_heap
 Desc: Create a heap.
 Params: None.
 Returns: None.
 Caveats: None.
*/

void os_init_heap( void )
{
   if ( true == os_heap_inited )
      return;

   memset( &os_heap_inited, 0, sizeof(os_heap_inited) );
   memset( &os_fs_hcb, 0, sizeof(os_fs_hcb) );

   lib_creat_malloc( &os_fs_hcb, "ionfs_heap", os_fs_heap, sizeof(os_fs_heap) );

   os_heap_inited = true;
}




/*
 Name: os_terminate_heap
 Desc: De-initialized a created heap.
 Params: None.
 Returns: None.
 Caveats: None.
*/

void os_terminate_heap( void )
{
   lib_delete_malloc( &os_fs_hcb );

   os_heap_inited = false;
}




/*
 Name: os_malloc
 Desc: Allocate a block with a specific size.
 Params:
   - size: A size of a block to be allocated.
 Returns:
   void*  value on success. The return value is address of a allocated block.
          NULL on fail.
 Caveats: None.
*/

void *os_malloc( uint32_t size )
{
   return lib_malloc( &os_fs_hcb, size );
}




/*
 Name: os_free
 Desc: Free a specific block
 Params:
   - size: Address of a block to be free.
 Returns: None.
 Caveats: None.
*/

void os_free( void *block )
{
   lib_free( &os_fs_hcb, block );
}
#endif




/*
 Name: os_init_sm
 Desc: Create a semaphore.
 Params: None.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: Initial count of created semaphore is 1.
*/

int32_t os_init_sm( void )
{

   char sm_name[16] = BASE_NAME " "/*index-string*/;
   int32_t i;


   if ( os_safe_mode )
      return IONFS_OK;

   if ( true == os_sm_inited )
      return IONFS_OK;

   for ( i = 0; i < SM_MAX; i++ ) {
      os_fs_sm_ptr[i] = (void *) &os_fs_sm[i];

      sm_name[IDX_POS] = (char)(i + '0');
 #if ( IONFS_OS & IONFS_OS_RTX_CRATUS )
      os_fs_sm_ptr[i] = osSemaphoreCreate(osSemaphore(os_fs_sm[i]),1);
 #endif
   }

   os_sm_inited = true;
   return IONFS_OK;
}




/*
 Name: os_terminate_sm
 Desc: Delete a semaphore.
 Params: None.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t os_terminate_sm( void )
{
   int32_t  i;


   if ( os_safe_mode )
      return IONFS_OK;

   for ( i = 0; i < SM_MAX; i++ ) {
      if ( os_fs_sm_ptr[i] ) {
	 #if ( IONFS_OS & IONFS_OS_RTX_CRATUS )			 
         if (osSemaphoreDelete((osSemaphoreId)os_fs_sm[i]) != 0 )
            return os_set_errno(IONFS_EOSD);
				 os_fs_sm_ptr[i] = NULL;
	#endif
      }
   }

   os_sm_inited = false;
   return IONFS_OK;
}




/*
 Name: os_obtain_sm
 Desc: Obtain a semaphore
 Params:
   - sem: Pointer to semaphore to be obtain.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t os_obtain_sm( void *sem )
{
   if ( os_safe_mode )
      return IONFS_OK;

	 
	  #if ( IONFS_OS & IONFS_OS_RTX_CRATUS )
   if ( 0 >  osSemaphoreWait( (osSemaphoreId)sem, 0) )
      return os_set_errno(IONFS_EOSD);
		#endif
   return IONFS_OK;
}




/*
 Name: os_release_sm
 Desc: Release a semaphore
 Params:
   - sem: Pointer to semaphore to be release.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t os_release_sm( void *sem )
{
   if ( os_safe_mode )
      return IONFS_OK;
 #if ( IONFS_OS & IONFS_OS_RTX_CRATUS )
   if (osSemaphoreRelease	( (osSemaphoreId)sem) != 0 )
      return os_set_errno(IONFS_EOSD);
 #endif
	 


   return IONFS_OK;
}




/*
 Name: os_set_safe_mode
 Desc: Set OS to safe-mode.
 Params:
   - is_safe: value of safe-mode to be set.
 Returns:
   int32_t  true sefe-mode is set.
            false sefe-mode isn't set.
 Caveats: None.
*/

bool_t os_set_safe_mode( bool_t is_safe )
{
   return os_safe_mode = is_safe;
}




/*
 Name: FSTimeStamp
 Desc: Get time of os.
 Params: None.
 Returns: None.
 Caveats: It usually used to get a random value.
*/

uint32_t FSTimeStamp( void )
{
   return 0x19770321;
}




/*
 Name: os_localtime
 Desc: Get the current time.
 Params: None.
 Returns:
   os_tm_t*  value This function returns the current time with os_tm_t format.
 Caveats: It depends on hardware system.
*/

os_tm_t *os_localtime( void )
{
   static os_tm_t lt;
#if defined( TLC_PLATFORM ) && defined( TARGET )  /* Target of Agere System. */
   DateTime dt = GetDateTime();


   lt.hour = dt.hour;
   lt.min = dt.min;
   lt.sec = dt.sec;
   lt.year = dt.year-1900;
   lt.mon = dt.month-1;
   lt.wday = dt.dayofweek;
   lt.mday = dt.day;
   lt.yday = 1;
   lt.isdst = 1;
#endif

#if ( IONFS_OS & IONFS_OS_LINUX )
   struct tm *local;
   time_t t=time(NULL);
   local=localtime(&t);
   lt.hour = local->tm_hour;
   lt.min = local->tm_min;
   lt.sec = local->tm_sec;
   lt.year = local->tm_year;
   lt.mon = local->tm_mon;
   lt.wday = local->tm_wday;
   lt.yday = local->tm_yday;
   lt.isdst = local->tm_isdst;
#endif

   return &lt;
}




/*
 Name: os_break
 Desc: When exceptions occur, this function is called.
 Params: None.
 Returns: None.
 Caveats: None.
*/

void os_break( void )
{
   #if defined( __arm ) && ( __ARMCC_VERSION > 210000 ) && !(defined(__TARGET_CPU_ARM920T) || defined(__TARGET_CPU_ARM7TDMI))
   __breakpoint(0xFE);
   #elif defined( WIN32 ) && !(defined(__CYGWIN__))
   __debugbreak();

   #elif defined( SYSSIM )
   char bbb;
   char* bt = 0;
   printf("os_break: AN ERROR HAS OCCURED IN THE IONFS FIRMWARE\n");
   printf("  os_break will now cause a Segmentation Fault to force the debugger to break\n");
   fflush(stdout);
   fprintf(stderr, "os_break: AN ERROR HAS OCCURED IN THE IONFS FIRMWARE\n");
   fprintf(stderr, "  os_break will now cause a Segmentation Fault to force the debugger to break\n");
   fflush(stderr);
   bbb = *bt;

   #else
   while ( 1 );
   #endif
}




/*
 Name: os_assert
 Desc: Cause the assertion.
 Params:
   - condition: Specify the condition of assertion.
 Returns: None.
 Caveats: None.
*/

void os_assert( bool_t condition )
{
   if ( false == condition ) {
      #if defined( WIN32 )
      assert(false);
      #else
      os_break();
      #endif
   }
}




/*
 Name: os_exit
 Desc: When exceptions occur, this function is called.
 Params:
   - exit_code: The exit code number.
 Returns: None.
 Caveats: None.
*/

void os_exit( uint32_t exit_code )
{
   #if defined( WIN32 )
   exit(1);
   #else
   os_break();
   #endif
}




/*
 Name: os_set_errno
 Desc: Set the error number at the global variable 'os_errno'.
 Params:
   - errno: the error number to be set.
 Returns:
   int32_t  -1 always.
 Caveats: None.
*/

int32_t os_set_errno( int32_t err_no )
{
   #if ( IONFS_OS & IONFS_OS_NUCLEUS )
   NU_TASK *task = NU_Current_Task_Pointer();
   #endif

   os_errno = (errno_t) err_no;

   #if ( IONFS_OS & IONFS_OS_ionOS )
   ion_set_usr_data( 0, (void *) err_no );
   #elif ( IONFS_OS & IONFS_OS_NUCLEUS )
   if ( task )
      ((int32_t *)task)[NU_TASK_SIZE-1] = err_no;
   #elif ( IONFS_OS & IONFS_OS_WIN32 )
   #endif

   return -1;
}




/*
 Name: os_get_errno
 Desc: Get the error number from the global variable 'os_errno'.
 Params: None.
 Returns:
   int32_t  value The return value is a error number lately.
 Caveats: None.
*/

int32_t os_get_errno( void )
{
   int32_t err = os_errno;


   #if ( IONFS_OS & IONFS_OS_ionOS )
   err = (int32_t) ion_get_usr_data( 0 );
   #elif ( IONFS_OS & IONFS_OS_NUCLEUS )
   NU_TASK *task = NU_Current_Task_Pointer();

   if ( task )
      err = ((int32_t *)task)[NU_TASK_SIZE-1];
   #elif ( IONFS_OS & IONFS_OS_WIN32 )
   #endif

   return err;
}

/*----------------------------------------------------------------------------
 END OF FILE
----------------------------------------------------------------------------*/


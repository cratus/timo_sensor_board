/*
$DESCRIPTION		: This file contains definitions for TIMO project

$Copyright			: CRATUS TECHNOLOGY INC, 2013-17

$Project				: BLUEBRAIN

$Author					: David

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIMO_H
#define __TIMO_H

/*
 * TIMO project
 */ 
 
//#define JITTER_TEST
#define SYNC_TEST
#endif /*__TIMO_H */

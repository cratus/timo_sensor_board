/*
$DESCRIPTION		: This file contains definitions for ADC functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_APP_MAIN_H
#define __BLUEBRAIN_APP_MAIN_H


/*
 */
	 #include <string.h>
	 #include "stm32f4xx_hal.h"
	 #include "cmsis_os.h"
	 #include "BlueBrain_IO.h"
	 #include "BlueBrain_init.h"
	 #include "BlueBrain_GPIO.h"
	 #include "BlueBrain_utilities.h"
	 #include "BlueBrain_DEMO_FFT.h"
	 					
	 #define UNUSED_VARIABLE(X)  					((void)(X))
	 #define AS_TS_ENABLE									1
	 #define TIMER0_INTERVAL							1000
	 #define FFT_DEMO											0
	 
	 static void blue_brain_system_init(); //Initializes all bluebrain peripherals

	 
	 extern RTC_HandleTypeDef hrtc;
	
	 #if FFT_DEMO
			static BB_FFT_Demo FFTDemo; 
	 #endif
	 
#endif /*__BLUEBRAIN_APP_MAIN_H */

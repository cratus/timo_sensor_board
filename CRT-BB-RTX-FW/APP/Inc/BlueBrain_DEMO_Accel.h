/*
$DESCRIPTION		: This file contains Accelerometer demo class declaration.

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_APP_ACCEL_DEMO_H
#define __BLUEBRAIN_APP_ACCEL_DEMO_H

#include "BlueBrain_Demo.h"
#include "BlueBrain_LIS2DH.h"

class BB_Accel_SensorDemo : public BlueBrain_Demo
{
	public:
		
		bool init(void);
	
		bool run(void);
	
		bool run_InLowPowerMode(void);
	
		bool run_InShutDownMode(void);
	
	private:
		LIS2DH Accel_Sensor;
};



#endif /*__BLUEBRAIN_APP_ACCEL_DEMO_H */
/*
$DESCRIPTION		: This is the main file which contains calling application level functions like initialization and run

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17
                : (C) COPYRIGHT STMicroelectronics

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Includes ------------------------------------------------------------------*/
#include "TIMO.h"
#include "stdint.h"
#include "RS_485.h"
#include "cmsis_os.h"
#include "BlueBrain_GPIO.h"
#include "BlueBrain_utilities.h"
#include "BlueBrain_io.h"


#ifdef __cplusplus
 extern "C" {
#endif 

void USART2_IRQHandler(void);

#ifdef __cplusplus
}
#endif

static void uart_RS485_Listen(void);
static void uart_RS485_Talk(void);

extern osThreadId	user_uart_task_id;  // TIMO - user UART task

/*----------------------------------------------------------------------------
  Notes:
  The length of the receive and transmit buffers must be a power of 2.
  Each buffer has a next_in and a next_out index.
  If next_in = next_out, the buffer is empty.
  (next_in - next_out) % buffer_size = the number of characters in the buffer.
 *----------------------------------------------------------------------------*/
#define TBUF_SIZE   512	     /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/
#define RBUF_SIZE   512      /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/

/*----------------------------------------------------------------------------
 *----------------------------------------------------------------------------*/
#if TBUF_SIZE < 2
#error TBUF_SIZE is too small.  It must be larger than 1.
#elif ((TBUF_SIZE & (TBUF_SIZE-1)) != 0)
#error TBUF_SIZE must be a power of 2.
#endif

#if RBUF_SIZE < 2
#error RBUF_SIZE is too small.  It must be larger than 1.
#elif ((RBUF_SIZE & (RBUF_SIZE-1)) != 0)
#error RBUF_SIZE must be a power of 2.
#endif

struct buf_st {
  unsigned int in;                                // Next In Index
  unsigned int out;                               // Next Out Index
  uint8_t buf [RBUF_SIZE];                       // Buffer
};

#if 0
struct buf_st_RS485 {
  unsigned int in;                                // Next In Index
  unsigned int out;                               // Next Out Index
  uint16_t buf [RBUF_SIZE];                       // Buffer
};
#endif
// User USART
static struct buf_st rbuf_user = { 0, 0, };
static struct buf_st tbuf_user = { 0, 0, };

#define SIO_RBUFLEN 	((unsigned short)(rbuf_user.in - rbuf_user.out))
#define SIO_TBUFLEN  	((unsigned short)(tbuf_user.in - tbuf_user.out))
	
static unsigned int tx_restart_user = 1;               // NZ if TX restart is required
/*
 * TIMO Project - User UART isr (USART2)
 */
/*----------------------------------------------------------------------------
  USART2_IRQHandler (User USART)
  Handles USART2 global interrupt request.
 *----------------------------------------------------------------------------*/
void USART2_IRQHandler (void) {
  volatile uint32_t IIR;
  struct buf_st *p;
	
	IIR = USART2->SR;
	if (IIR & UART_FLAG_RXNE) {
		p = &rbuf_user;	

		if (((p->in - p->out) & ~(RBUF_SIZE-1)) == 0) {
			p->buf [p->in & (RBUF_SIZE-1)] = (USART2->DR & 0x01FF);
			p->in++;
		}		
		osSignalSet (user_uart_task_id, 0x0001); // signal user UART task that character is ready.	
		USART2->SR &= ~UART_FLAG_RXNE;	          // clear interrupt	
	}
	if (IIR & UART_FLAG_TXE) {		
		USART2->SR &= ~UART_FLAG_TXE;	          // clear interrupt		
		
		p = &tbuf_user;
		if (p->in != p->out) 
		{	
			USART2->DR = p->buf [p->out & (TBUF_SIZE-1)];		
			tx_restart_user = 0;
			p->out++;	
		}
		else
	  {
			tx_restart_user = 1;
			USART2->CR1 &= ~UART_FLAG_TXE;		      // disable TX interrupt if nothing to send
			uart_RS485_Listen(); // Tri-state TX line	
		}	
	} // end if ((IIR & UART_FLAG_TXE))

} // end void USART2_IRQHandler (void)

/*------------------------------------------------------------------------------
  RS_485_buffer_Init
  initialize the buffers
 *------------------------------------------------------------------------------*/
void RS_485_buffer_Init (void) {

  tbuf_user.in = 0;                                    // Clear com buffer indexes
  tbuf_user.out = 0;
  tx_restart_user = 1;

  rbuf_user.in = 0;
  rbuf_user.out = 0;
	
	uart_RS485_Listen(); // Tri-state TX line
}

/*------------------------------------------------------------------------------
  RS_485_sendChar
  transmit a character via User USART
 *------------------------------------------------------------------------------*/
void RS_485_sendChar(uint16_t c) {
  struct buf_st *p = &tbuf_user;

  p->buf [p->in & (TBUF_SIZE - 1)] = c;    // Add data to the transmit buffer.
  p->in++;
		
  if (tx_restart_user)
	{
    tx_restart_user = 0;
	  USART2->CR1 |= UART_IT_TXE;		         // enable TX interrupt
	}
}

void RS_485_sendAddressCharacter(uint8_t ch)
{
  uart_RS485_Talk(); // Assert TX line
				
	RS_485_sendChar(ch); // echo character
	RS_485_sendChar(0x00); // send dummy terminator character
}

void RS_485_sendPacket(timoPacket pk)
{
  uint8_t *tx_pkt, i;
  uart_RS485_Talk(); // Assert TX line
  for (i=0, tx_pkt = (uint8_t *)&pk; i<sizeof(timoPacket); i++)
		RS_485_sendChar(*tx_pkt++);
	RS_485_sendChar(0x00); // send dummy terminator character
}

/*------------------------------------------------------------------------------
  RS_485_getChar
  receive a character
 *------------------------------------------------------------------------------*/
int RS_485_getChar(void) {
  struct buf_st *p = &rbuf_user;
	int ch;
	
	uint16_t int_enable_reg_save;
	
	int_enable_reg_save = USART2->CR1;          // save current interrupt state
	
  USART2->CR1 &= ~USART_CR1_RXNEIE;	          // disable receive interrupt	
	
  if (SIO_RBUFLEN == 0)
    ch = -1;
	else
    ch = (p->buf [(p->out++) & (RBUF_SIZE - 1)]);
	USART2->CR1 = int_enable_reg_save;         // restore previous interrupt state
	return ch;
}



// proof-of-concept connections

// signal(s) Pin#  GPIO   Name
//
//   RO      13    ----   USART2 RX 
//   DI      14    ----   USART2 TX
//  /RE,DE   22    GPIO5 /RX ENABLE, TX_ENABLE
//

static void uart_RS485_Listen()
{

	
 
	GPIO_reset(GPIO_5);

}

static void uart_RS485_Talk()
{
	GPIO_set(GPIO_5);

}


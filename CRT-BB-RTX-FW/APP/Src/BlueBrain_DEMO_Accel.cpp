/*
$DESCRIPTION		: This file contains APIs to easily initialize accelerometer sensor 
									and get the accelerometer data from the sensor LIS2DH.

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

#include "BlueBrain_DEMO_Accel.h"
#include "BlueBrain_IO.h"

bool 
BB_Accel_SensorDemo :: init(void)
{
	if(Accel_Sensor.init())
	{
		DEBUG_UART.println(" SUCCESS - Accelerometer sensor Initialization");
		return true;
	}	
	else
	{
		DEBUG_UART.println(" FAILURE - Accelerometer sensor Initialization");
		return false;
	}
}


bool 
BB_Accel_SensorDemo :: run(void)
{
		int16_t accel_x,accel_y,accel_z;
		if(Accel_Sensor.getMotionData(&accel_x,&accel_y,&accel_z))
		{
			printf("Accel -> X,Y,Z -- %d,%d,%d\n", accel_x,accel_y,accel_z);
			return true;
		}
		else					// Means error in collecting data
		{
			return false;
		}
}

bool 
BB_Accel_SensorDemo :: run_InLowPowerMode(void)
{
		int16_t accel_x,accel_y,accel_z;
	
	  if(!Accel_Sensor.setMode(LIS2DH_LOW_POWER))   // Let's set the mode to Low Power Mode
		{
			return false;
		}
		
		if(Accel_Sensor.getMotionData(&accel_x,&accel_y,&accel_z))
		{
			printf("Accel -> X,Y,Z -- %d,%d,%d\n", accel_x,accel_y,accel_z);
			return true;
		}
		else					// Means error in collecting data
		{
			return false;
		}
}

bool 
BB_Accel_SensorDemo :: run_InShutDownMode(void)
{
		int16_t accel_x,accel_y,accel_z;
	
	  if(!Accel_Sensor.setMode(LIS2DH_POWER_DOWN))   // Let's set the mode to Power Down mode 
		{
			return false;
		}
		
		if(Accel_Sensor.getMotionData(&accel_x,&accel_y,&accel_z))
		{
			printf("Accel -> X,Y,Z -- %d,%d,%d\n", accel_x,accel_y,accel_z);
			return true;
		}
		else					// Means error in collecting data
		{
			return false;
		}
}

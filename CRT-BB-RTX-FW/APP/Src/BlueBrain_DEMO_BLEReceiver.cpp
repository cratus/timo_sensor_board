/*
$DESCRIPTION		: This file contains APIs to test the BLE implementation with
									the NRF controller via UART

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

#include "BlueBrain_DEMO_BLEReceiver.h"
#include "BlueBrain_IO.h"

bool 
BB_BLEReceive_Demo :: init(void)
{
	// Currently nothing to init for it.
	return true;
}


bool 
BB_BLEReceive_Demo :: run(void)
{
	
	uint8_t status = HAL_UART_Receive(BLE.get_uart(),&received_data,1,200);
	if(received_data=='1')
	{
		GPIO_reset(GPIO_0|GPIO_1|GPIO_2|GPIO_3|GPIO_4|GPIO_5|GPIO_6|GPIO_7);
	}
	else if(received_data=='2')
	{
		GPIO_set(GPIO_0|GPIO_1|GPIO_2|GPIO_3|GPIO_4|GPIO_5|GPIO_6|GPIO_7);
	}
	received_data = 0;
	return true;
}



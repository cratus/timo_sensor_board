/*
$DESCRIPTION		: This file contains APIs to easily initialize temperature sensor and 
									get the current temperature data from the sensor SI7050 using template BlueBrain_Demo class.

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

#include "BlueBrain_DEMO_Temp.h"
#include "BlueBrain_IO.h"


bool 
BB_Temp_SensorDemo :: init(void)
{
	
	if(Temp_Sensor.init())
		{
		DEBUG_UART.println(" SUCCESS - Temperature sensor Initialization");
		return true;
	  }
		
	else
	{
		DEBUG_UART.println(" FAILURE - Temperature sensor Initialization");
		return false;
	}
}


bool 
BB_Temp_SensorDemo :: run(void)
{
		float temperature = 0;
		temperature = Temp_Sensor.getTemperatureFahren();       // Tested Working fine..
		printf("Temp in Fahren: %f\n",temperature); 
		temperature = Temp_Sensor.getTemperatureCenti();         // Tested Working fine..
		printf("Temp in Centi: %f\n",temperature);
		return true;
}
